﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Properties.Resources
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace RorAdmin.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (RorAdmin.Properties.Resources.resourceMan == null)
          RorAdmin.Properties.Resources.resourceMan = new ResourceManager("RorAdmin.Properties.Resources", typeof (RorAdmin.Properties.Resources).Assembly);
        return RorAdmin.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return RorAdmin.Properties.Resources.resourceCulture;
      }
      set
      {
        RorAdmin.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
