﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.MYPEditorsView
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MYPLib;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
  public class MYPEditorsView : UserControl, IPersistControl
  {
    private IContainer components = (IContainer) null;
    private TabControl tabControl1;
    private TabPage tabPage2;
    private TabPage tabPage3;
    private TabPage tabPage4;
    private TabPage tabPage5;

    public MYPEditorsView()
    {
      this.InitializeComponent();
    }

    public void LoadSettings(Config config)
    {
    }

    public void SaveSettings(Config config)
    {
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.tabControl1 = new TabControl();
      this.tabPage2 = new TabPage();
      this.tabPage3 = new TabPage();
      this.tabPage4 = new TabPage();
      this.tabPage5 = new TabPage();
      this.tabControl1.SuspendLayout();
      this.SuspendLayout();
      this.tabControl1.Controls.Add((Control) this.tabPage2);
      this.tabControl1.Controls.Add((Control) this.tabPage3);
      this.tabControl1.Controls.Add((Control) this.tabPage4);
      this.tabControl1.Controls.Add((Control) this.tabPage5);
      this.tabControl1.Dock = DockStyle.Fill;
      this.tabControl1.Location = new Point(0, 0);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new Size(509, 333);
      this.tabControl1.TabIndex = 0;
      this.tabPage2.Location = new Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new Padding(3);
      this.tabPage2.Size = new Size(501, 307);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Abilities";
      this.tabPage2.UseVisualStyleBackColor = true;
      this.tabPage3.Location = new Point(4, 22);
      this.tabPage3.Name = "tabPage3";
      this.tabPage3.Padding = new Padding(3);
      this.tabPage3.Size = new Size(501, 307);
      this.tabPage3.TabIndex = 2;
      this.tabPage3.Text = "Figleaf";
      this.tabPage3.UseVisualStyleBackColor = true;
      this.tabPage4.Location = new Point(4, 22);
      this.tabPage4.Name = "tabPage4";
      this.tabPage4.Padding = new Padding(3);
      this.tabPage4.Size = new Size(501, 307);
      this.tabPage4.TabIndex = 3;
      this.tabPage4.Text = "Art";
      this.tabPage4.UseVisualStyleBackColor = true;
      this.tabPage5.Location = new Point(4, 22);
      this.tabPage5.Name = "tabPage5";
      this.tabPage5.Padding = new Padding(3);
      this.tabPage5.Size = new Size(501, 307);
      this.tabPage5.TabIndex = 4;
      this.tabPage5.Text = "Audio";
      this.tabPage5.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.tabControl1);
      this.Name = nameof (MYPEditorsView);
      this.Size = new Size(509, 333);
      this.tabControl1.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
