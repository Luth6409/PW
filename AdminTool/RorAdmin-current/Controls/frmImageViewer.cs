﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmImageViewer
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
  public class frmImageViewer : BaseViewer
  {
    private bool _disableUpdates = false;
    private IContainer components = (IContainer) null;
    private PictureBox pictureBox1;
    private StatusStrip statusStrip1;
    private ToolStripStatusLabel lblSizeInfo;
    private Panel panel1;

    public frmImageViewer()
    {
      this.InitializeComponent();
    }

    public override void LoadEntry(MYPManager manager, MYP.AssetInfo entry)
    {
    }

    public override void Save()
    {
    }

    private void scintilla_TextChanged(object sender, EventArgs e)
    {
      if (!this._disableUpdates)
        ;
    }

    private void InitializeComponent()
    {
      this.pictureBox1 = new PictureBox();
      this.statusStrip1 = new StatusStrip();
      this.lblSizeInfo = new ToolStripStatusLabel();
      this.panel1 = new Panel();
      ((ISupportInitialize) this.pictureBox1).BeginInit();
      this.statusStrip1.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      this.pictureBox1.Location = new Point(0, 0);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new Size(100, 50);
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      this.statusStrip1.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.lblSizeInfo
      });
      this.statusStrip1.Location = new Point(0, 475);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Size = new Size(820, 22);
      this.statusStrip1.TabIndex = 1;
      this.statusStrip1.Text = "statusStrip1";
      this.lblSizeInfo.Name = "lblSizeInfo";
      this.lblSizeInfo.Size = new Size(118, 17);
      this.lblSizeInfo.Text = "toolStripStatusLabel1";
      this.panel1.AutoScroll = true;
      this.panel1.BackColor = Color.DarkGray;
      this.panel1.Controls.Add((Control) this.pictureBox1);
      this.panel1.Dock = DockStyle.Fill;
      this.panel1.Location = new Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new Size(820, 475);
      this.panel1.TabIndex = 2;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.panel1);
      this.Controls.Add((Control) this.statusStrip1);
      this.Name = nameof (frmImageViewer);
      ((ISupportInitialize) this.pictureBox1).EndInit();
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
