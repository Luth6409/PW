﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.RegionInfo
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

using System.Collections.Generic;

namespace WarZoneLib
{
  public class RegionInfo
  {
    public Dictionary<uint, ZoneInfo> Zones = new Dictionary<uint, ZoneInfo>();
    public uint ID;
  }
}
