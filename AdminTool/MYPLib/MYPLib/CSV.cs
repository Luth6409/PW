﻿// Decompiled with JetBrains decompiler
// Type: MypLib.CSV
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MypLib
{
  public class CSV
  {
    public List<List<string>> Lines = new List<List<string>>();
    public Dictionary<string, List<string>> _keyedRows = new Dictionary<string, List<string>>();
    private string _commentSection;

    public int RowIndex { get; set; }

    public List<string> Row
    {
      get
      {
        return this.ReadRow();
      }
    }

    public bool EOF
    {
      get
      {
        return this.RowIndex == this.Lines.Count - 1;
      }
    }

    public CSV()
    {
      this.RowIndex = 0;
    }

    public string GetKeyedValue(object rowID, int colIndex, string defaultValue = "")
    {
      string key = rowID.ToString();
      if (this._keyedRows.ContainsKey(key) && colIndex < this._keyedRows[key].Count)
        return this._keyedRows[key][colIndex];
      return defaultValue;
    }

    public static List<string> QuotedCSVToList(string line, char quote = '"', char splitter = ',')
    {
      List<string> stringList = new List<string>();
      char[] charArray = line.ToCharArray();
      bool flag = false;
      string str = "";
      for (int index = 0; index < charArray.Length; ++index)
      {
        if ((int) charArray[index] == (int) quote && !flag)
          flag = true;
        else if ((int) charArray[index] == (int) quote & flag)
          flag = false;
        else if ((int) charArray[index] == (int) splitter && !flag)
        {
          stringList.Add(str);
          str = "";
        }
        else
          str = !flag ? str + charArray[index].ToString() : str + charArray[index].ToString();
      }
      if (str.Length > 0)
        stringList.Add(str);
      return stringList;
    }

    public CSV(string text, string commentSection = null, bool quoted = false, int idCol = -1)
    {
      this._commentSection = commentSection;
      string str1 = "";
      string str2 = text;
      char[] separator = new char[1]{ '\n' };
      int num = 1;
      foreach (string line in ((IEnumerable<string>) str2.Split(separator, (StringSplitOptions) num)).ToList<string>())
      {
        if (this._commentSection != null && line.StartsWith(this._commentSection))
          str1 = line.Replace(",", "").Replace(";", "");
        if (!line.StartsWith(";"))
        {
          List<string> stringList = new List<string>();
          if (quoted)
          {
            stringList = CSV.QuotedCSVToList(line, '"', ',');
          }
          else
          {
            string str3 = line;
            char[] chArray = new char[1]{ ',' };
            foreach (string str4 in str3.Split(chArray))
              stringList.Add(str4);
          }
          if (this._commentSection != null && str1 != "")
            stringList.Add(str1);
          this.Lines.Add(stringList);
          if (idCol > -1 && idCol < stringList.Count)
            this._keyedRows[stringList[idCol]] = stringList;
        }
      }
      this.RowIndex = 0;
    }

    public void MoveToEnd()
    {
      this.RowIndex = this.Lines.Count - 1;
    }

    public void NewRow()
    {
      this.Lines.Insert(this.RowIndex + 1, new List<string>());
    }

    public void RemoveRow()
    {
      this.Lines.RemoveAt(this.RowIndex);
    }

    public void Remove(List<int> rows)
    {
      List<List<string>> stringListList = new List<List<string>>();
      for (int index = 0; index < this.Lines.Count; ++index)
      {
        if (!rows.Contains(index))
          stringListList.Add(this.Lines[index]);
      }
      this.Lines = stringListList;
      if (this.RowIndex <= this.Lines.Count - 1)
        return;
      this.RowIndex = stringListList.Count - 1;
    }

    public void WriteCol(int col, string value)
    {
      List<string> line = this.Lines[this.RowIndex];
      if (line.Count - 1 < col)
      {
        int num = col - line.Count;
        for (int index = 0; index <= num; ++index)
          line.Add("");
      }
      line[col] = value;
    }

    public List<string> ReadRow()
    {
      return this.Lines[this.RowIndex];
    }

    public void NextRow()
    {
      ++this.RowIndex;
    }

    public int ReadInt32(int col, int defaultValue = 0)
    {
      int result = 0;
      if (col < this.Lines[this.RowIndex].Count && int.TryParse(this.Lines[this.RowIndex][col], out result))
        return result;
      return defaultValue;
    }

    public int PeakInt32(int row, int col, int defaultValue = -1)
    {
      if (row >= this.Lines.Count)
        return defaultValue;
      int result = 0;
      int.TryParse(this.Lines[row][col], out result);
      return result;
    }

    public float ReadFloat(int col)
    {
      float result = 0.0f;
      if (col < this.Lines[this.RowIndex].Count && float.TryParse(this.Lines[this.RowIndex][col], out result))
        return result;
      return 0.0f;
    }

    public string ReadString(int col)
    {
      return this.Lines[this.RowIndex][col];
    }

    public string ToText()
    {
      StringBuilder stringBuilder = new StringBuilder();
      foreach (List<string> line in this.Lines)
      {
        for (int index = 0; index < line.Count; ++index)
        {
          if (index < line.Count - 1)
            stringBuilder.Append(line[index] + ",");
          else
            stringBuilder.Append(line[index]);
        }
        stringBuilder.Append("\n");
      }
      return stringBuilder.ToString();
    }
  }
}
