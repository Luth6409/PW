﻿// Decompiled with JetBrains decompiler
// Type: MypLib.PCX
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;
using System.IO;

namespace MypLib
{
  public class PCX
  {
    private byte _mfr;
    private byte _version;
    private byte _encoding;
    private byte _bpp;
    private byte _nplanes;
    private ushort _xmin;
    private ushort _xmax;
    private ushort _ymin;
    private ushort _ymax;
    private ushort _hres;
    private ushort _vres;
    private ushort _bytesPerLine;
    private byte[] _data;

    public int Width
    {
      get
      {
        return (int) this._xmax + 1 - (int) this._xmin;
      }
    }

    public int Height
    {
      get
      {
        return (int) this._ymax + 1 - (int) this._ymin;
      }
    }

    public byte GetValue(int x, int y)
    {
      return this.GetValue(x, y, 0);
    }

    public byte GetValue(int x, int y, int plane)
    {
      if (plane >= (int) this._nplanes)
        throw new ArgumentOutOfRangeException();
      return this._data[y * (int) this._nplanes * this.Width + (int) this._nplanes * x + plane];
    }

    public void Load(Stream stream)
    {
      this.LoadFromReader(new BinaryReader(stream));
    }

    private void LoadFromReader(BinaryReader reader)
    {
      this._mfr = reader.ReadByte();
      this._version = reader.ReadByte();
      this._encoding = reader.ReadByte();
      this._bpp = reader.ReadByte();
      this._xmin = reader.ReadUInt16();
      this._ymin = reader.ReadUInt16();
      this._xmax = reader.ReadUInt16();
      this._ymax = reader.ReadUInt16();
      this._hres = reader.ReadUInt16();
      this._vres = reader.ReadUInt16();
      reader.ReadBytes(48);
      int num1 = (int) reader.ReadByte();
      this._nplanes = reader.ReadByte();
      this._bytesPerLine = reader.ReadUInt16();
      int num2 = (int) reader.ReadUInt16();
      reader.ReadBytes(58);
      byte[] row = new byte[(int) this._nplanes * (int) this._bytesPerLine];
      this._data = new byte[this.Width * this.Height * (int) this._nplanes];
      for (int index1 = 0; index1 < this.Height; ++index1)
      {
        this.DecodeRow(row, reader);
        int index2 = index1 * this.Width * (int) this._nplanes;
        row.CopyTo((Array) this._data, index2);
      }
    }

    private void DecodeRow(byte[] row, BinaryReader reader)
    {
      byte num1 = 0;
      int num2 = 0;
      int num3 = 0;
      while (num2 < row.Length)
      {
        if (num3 > 0)
        {
          row[num2++] = num1;
          --num3;
        }
        else
        {
          num1 = reader.ReadByte();
          if (((int) num1 & 192) == 192)
          {
            num3 = (int) num1 & 63;
            num1 = reader.ReadByte();
          }
          else
          {
            num3 = 0;
            row[num2++] = num1;
          }
        }
      }
    }
  }
}
