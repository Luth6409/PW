﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.AbilityComponent
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.Collections.Generic;
using System.IO;

namespace MYPLib
{
  public class AbilityComponent
  {
    public AbilityBin Ability;
    public string Description;

    public byte Index { get; set; }

    public ushort A11_ComponentID { get; internal set; }

    public ushort A00 { get; set; }

    public List<ExtData> A02_Data { get; set; } = new List<ExtData>();

    public short[] Values { get; set; } = new short[16];

    public int[] Multipliers { get; set; } = new int[8];

    public uint A05 { get; set; }

    public uint A06_Duration { get; set; }

    public uint A07 { get; set; }

    public uint A08 { get; set; }

    public ComponentOP A09_Operation { get; set; }

    public uint A10_Interval { get; set; }

    public ushort A12_Radius { get; set; }

    public ushort A13 { get; set; }

    public ushort A14 { get; set; }

    public ushort A15 { get; set; }

    public byte A16 { get; set; }

    public BonusType BonusType { get; internal set; }

    public string LinkedAbility { get; internal set; }

    public AbilityComponent(ushort ID, AbilityComponent cloneFrom = null)
    {
      this.A11_ComponentID = ID;
      if (cloneFrom == null)
        return;
      this.A00 = cloneFrom.A00;
      foreach (ExtData extData in cloneFrom.A02_Data)
        this.A02_Data.Add(new ExtData()
        {
          Val1 = extData.Val1,
          Val2 = extData.Val2,
          Val3 = extData.Val3,
          Val4 = extData.Val4,
          Val5 = extData.Val5,
          Val6 = extData.Val6,
          Val7 = extData.Val7,
          Val8 = extData.Val8,
          Val9 = extData.Val9
        });
      for (int index = 0; index < 16; ++index)
        this.Values[index] = cloneFrom.Values[index];
      for (int index = 0; index < 8; ++index)
        this.Multipliers[index] = cloneFrom.Multipliers[index];
      this.A05 = cloneFrom.A05;
      this.A06_Duration = cloneFrom.A06_Duration;
      this.A07 = cloneFrom.A07;
      this.A08 = cloneFrom.A08;
      this.A09_Operation = cloneFrom.A09_Operation;
      this.A10_Interval = cloneFrom.A10_Interval;
      this.A12_Radius = cloneFrom.A12_Radius;
      this.A13 = cloneFrom.A13;
      this.A14 = cloneFrom.A14;
      this.A15 = cloneFrom.A15;
      this.A16 = cloneFrom.A16;
      this.Description = cloneFrom.Description;
      this.BonusType = cloneFrom.BonusType;
      this.LinkedAbility = cloneFrom.LinkedAbility;
    }

    public AbilityComponent(Stream stream)
    {
      this.A00 = PacketUtil.GetUint16R(stream);
      for (int index = 0; index < 8; ++index)
      {
        if (PacketUtil.GetUint32R(stream) == 2863311530U)
          this.A02_Data.Add(new ExtData()
          {
            Val1 = (int) PacketUtil.GetUint32R(stream),
            Val2 = (int) PacketUtil.GetUint32R(stream),
            Val3 = (int) PacketUtil.GetUint32R(stream),
            Val4 = (int) PacketUtil.GetUint32R(stream),
            Val5 = (int) PacketUtil.GetUint32R(stream),
            Val6 = (int) PacketUtil.GetUint32R(stream),
            Val7 = (int) PacketUtil.GetUint32R(stream),
            Val8 = (int) PacketUtil.GetUint32R(stream),
            Val9 = PacketUtil.GetUint8(stream)
          });
      }
      for (int index = 0; index < 16; ++index)
        this.Values[index] = (short) PacketUtil.GetUint16R(stream);
      for (int index = 0; index < 8; ++index)
        this.Multipliers[index] = (int) (short) PacketUtil.GetUint32R(stream);
      this.A05 = PacketUtil.GetUint32R(stream);
      this.A06_Duration = PacketUtil.GetUint32R(stream);
      this.A07 = PacketUtil.GetUint32R(stream);
      this.A08 = PacketUtil.GetUint32R(stream);
      this.A09_Operation = (ComponentOP) PacketUtil.GetUint32R(stream);
      this.A10_Interval = PacketUtil.GetUint32R(stream);
      this.A11_ComponentID = PacketUtil.GetUint16R(stream);
      int a11ComponentId = (int) this.A11_ComponentID;
      this.A12_Radius = PacketUtil.GetUint16R(stream);
      this.A13 = PacketUtil.GetUint16R(stream);
      this.A14 = PacketUtil.GetUint16R(stream);
      this.A15 = PacketUtil.GetUint16R(stream);
      this.A16 = PacketUtil.GetUint8(stream);
    }

    public void Save(Stream stream)
    {
      PacketUtil.WriteUInt16R(stream, this.A00);
      for (int index = 0; index < 8; ++index)
      {
        if (index < this.A02_Data.Count)
        {
          PacketUtil.WriteUInt32R(stream, 2863311530U);
          this.A02_Data[index].Save(stream);
        }
        else
          PacketUtil.WriteUInt32R(stream, 0U);
      }
      for (int index = 0; index < 16; ++index)
        PacketUtil.WriteUInt16R(stream, (ushort) this.Values[index]);
      for (int index = 0; index < 8; ++index)
        PacketUtil.WriteUInt32R(stream, (uint) this.Multipliers[index]);
      PacketUtil.WriteUInt32R(stream, this.A05);
      PacketUtil.WriteUInt32R(stream, this.A06_Duration);
      PacketUtil.WriteUInt32R(stream, this.A07);
      PacketUtil.WriteUInt32R(stream, this.A08);
      PacketUtil.WriteUInt32R(stream, (uint) this.A09_Operation);
      PacketUtil.WriteUInt32R(stream, this.A10_Interval);
      PacketUtil.WriteUInt16R(stream, this.A11_ComponentID);
      PacketUtil.WriteUInt16R(stream, this.A12_Radius);
      PacketUtil.WriteUInt16R(stream, this.A13);
      PacketUtil.WriteUInt16R(stream, this.A14);
      PacketUtil.WriteUInt16R(stream, this.A15);
      PacketUtil.WriteByte(stream, this.A16);
    }
  }
}
