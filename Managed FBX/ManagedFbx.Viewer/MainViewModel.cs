﻿using GalaSoft.MvvmLight;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace ManagedFbx.Viewer
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            OpenFile = new DelegateCommand(this.OpenFileExecute);
            CloseFile = new DelegateCommand<FileViewModel>(this.CloseFileExecute);
            Exit = new DelegateCommand(Application.Current.Shutdown);
        }

        [Dependency]
        public IUnityContainer Container { get; set; }

        [Dependency]
        public IRegionManager RegionManager { get; set; }

        public ICommand OpenFile { get; private set; }
        public ICommand CloseFile { get; private set; }
        public ICommand Exit { get; private set; }

        public IList<FileViewModel> FilesOpened { get; } = new List<FileViewModel>();
        public IList<ContentViewModel> ContentOpenend { get; } = new List<ContentViewModel>();

        private void OpenFileExecute()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "FBX files (*.fbx)|*.fbx"
            };
            var results = dialog.ShowDialog();
            if (results.HasValue && results.Value)
            {
                FileViewModel fvm = null;
                try
                {
                    fvm = Container.Resolve<FileViewModel>(new ParameterOverride("fileName", dialog.FileName).OnType<FileViewModel>());
                    FilesOpened.Add(fvm);
                    var region = RegionManager.Regions[RegionNames.FilesOpenedRegion];
                    region.Add(fvm.View);
                    region.Activate(fvm.View);

                    var cvm = Container.Resolve<ContentViewModel>(new ParameterOverride("fileName", dialog.FileName).OnType<ContentViewModel>());
                    ContentOpenend.Add(cvm);
                    region = RegionManager.Regions[RegionNames.ContentViewRegion];
                    region.Add(cvm.View);
                    region.Activate(cvm.View);

                    fvm.ContentViewModel = cvm;
                    cvm.Scene = fvm.Scene;
                    cvm.CreateViewPort();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + "\r\n" + e.StackTrace, "Exception!", MessageBoxButton.OK, MessageBoxImage.Error);

                    if (fvm != null)
                        CloseFileExecute(fvm);
                }
            }
        }

        private void CloseFileExecute(FileViewModel viewModel)
        {
            if (viewModel == null)
                return;
            if (viewModel.ContentViewModel != null)
            {
                RegionManager.Regions[RegionNames.ContentViewRegion].Remove(viewModel.ContentViewModel.View);
                ContentOpenend.Remove(viewModel.ContentViewModel);
            }

            RegionManager.Regions[RegionNames.FilesOpenedRegion].Remove(viewModel.View);
            FilesOpened.Remove(viewModel);
        }
    }
}
