#pragma once

#include <fbxsdk.h>
#include "../Property.h"

using namespace System;

namespace UnexpectedBytes
{
	public value struct Vector4
	{
	public:

		property double X;
		property double Y;
		property double Z;
		property double W;

		Vector4(double x, double y, double z, double w)
		{
			X = x;
			Y = y;
			Z = z;
			W = w;
		}

		operator FbxDouble4()
		{
			return FbxDouble4(X, Y, Z, W);
		}

		virtual String^ ToString() override
		{
			return String::Format("{0}, {1}, {2}, {3}", Math::Round(X, 3), Math::Round(Y, 3), Math::Round(Z, 3), Math::Round(W, 3));
		}

		static Vector4 operator *(Vector4 self, float value)
		{
			self.X *= value;
			self.Y *= value;
			self.Z *= value;
			self.W *= value;
			return self;
		}

	internal:

		Vector4(FbxDouble4 fbxDouble)
		{
			X = fbxDouble[0];
			Y = fbxDouble[1];
			Z = fbxDouble[2];
			W = fbxDouble[3];
		}

	};
}