﻿using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{
    public class Vector2F
    {
        public float mX;
        public float mY;

        public Vector2F()
        {
            mX = 0.0f;
            mY = 0.0f;
        }

        public void ReadIn(BinaryReader iStream)
        {
            mX = iStream.ReadSingle();
            mY = iStream.ReadSingle();
        }

        public void WriteOut(BinaryWriter iStream)
        {
            iStream.Write(mX);
            iStream.Write(mY);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += Marshal.SizeOf(mX);
            vSize += Marshal.SizeOf(mY);
            return vSize;
        }
        public override string ToString()
        {
            return $"{mX}, {mY}";
        }

    }
}
