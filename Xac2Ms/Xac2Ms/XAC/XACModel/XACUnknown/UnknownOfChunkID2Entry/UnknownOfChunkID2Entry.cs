﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{
  public class XACUnknownOfChunkID2Entry
  {
    public float  mValue1;
    public UInt32 mValue2;
    public float  mValue3;
    public UInt32 mValue4;

    public XACUnknownOfChunkID2Entry(float iValue1, UInt32 iValue2, float iValue3, UInt32 iValue4)
    {
      mValue1 = iValue1;
      mValue2 = iValue2;
      mValue3 = iValue3;
      mValue4 = iValue4;
    }

    public override string ToString()
    {
      string vTheString = "XACUnknownOfChunkID2Entry Value1(float):" + mValue1.ToString() + " Value2(UInt32):" + mValue2.ToString() + " Value3(float):" + mValue3.ToString() + " Value4(UInt32):" + mValue4.ToString();
      return vTheString;
    }
  }

}