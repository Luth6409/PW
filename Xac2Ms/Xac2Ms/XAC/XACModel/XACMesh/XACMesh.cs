﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{

    public class Mesh
    {

        public List<XACFaceGroup> mFaceGroupList;
        public List<XACVertice> mVerticesList;
        public List<XACNormal> mNormalsList;
        public List<XACUVCoord> mUVCoordsList;
        public List<XACWeight> mWeightsList;
        public List<XACVerticeColor> mVerticeColorList;

        public Mesh()
        {
            mFaceGroupList = new List<XACFaceGroup>();
            mVerticesList = new List<XACVertice>();
            mNormalsList = new List<XACNormal>();
            mUVCoordsList = new List<XACUVCoord>();
            mWeightsList = new List<XACWeight>();
            mVerticeColorList = new List<XACVerticeColor>();
        }
    }



}