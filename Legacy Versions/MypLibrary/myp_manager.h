#pragma once

#include "platform.h"

#include "myp.h"

class MypManager
{
public:
    MypManager(LogCallback logCB, ProgressCallback progressCB);
    uint32_t Load(std::string path);
    bool IsLoaded() const;
    std::optional<std::vector<int32_t>> GetHashes();
    std::optional<uint32_t> GetHash(Myp* Myp);
    void Close();
    void Save();
    std::optional<Myp*> GetMyp(std::string name, bool create);
    std::optional<std::vector<Myp*>> GetMyps(bool create);
    uint8_t* GetAssetData(const char* name);
    uint8_t* GetAssetData(const char* name, int& size, Myp* firstArchive = NULL);

private:
    LogCallback _logCallBack;
    ProgressCallback _progressCallBack;
	std::map<Archive, Myp*> _myps;
	std::map<int, uint32_t> _hashes;
    std::string _path;

};