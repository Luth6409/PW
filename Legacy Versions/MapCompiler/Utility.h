
#pragma once

using namespace System;

#include <string>

#undef PI

ref class Utility
{
public:

	static String ^FormatSize(long size)
	{
		if (size < 1024)
			return size + "B";
		if (size < 1024 * 1024)
			return (size >> 10) + "KB";
		if (size < 1024 * 1024 * 1024)
			return (size >> 20) + "MB";
		return (size >> 30) + "GB";
	}

	template <typename T>
	static T Clamp(T value, T low, T high)
	{
		if (high < low)
			high;// throw gcnew ArgumentException();
		if (value < low)
			return low;
		if (value > high)
			return high;
		return value;
	}

	template <typename T>
	static T ToRadians(T degrees)
	{
		return (T)(degrees * Math::PI / 180.0);
	}

	static std::string MarshalString(System::String ^str)
	{
		std::string tmp;
		tmp.resize(str->Length);
		for (int i = 0; i < str->Length; i++)
			tmp[i] = static_cast<char>(str[i]);
		return tmp;
	}

	static String ^MarshalString(const std::string &str)
	{
		return gcnew System::String(str.c_str(), 0, static_cast<int>(str.size()));
	}

	static bool EqualsIgnoreCase(const std::string &str0, const std::string &str1)
	{
		if (str0.length() != str1.length())
			return false;
		return _stricmp(str0.c_str(), str1.c_str()) == 0;
	}

	static String ^Utility::GetTempOutputPath(String ^path)
	{
		Random ^r = gcnew Random();
		int value = r->Next(99999);
		return path + value.ToString()->PadLeft(5, '0');
	}

	static String ^GetToken(String ^&str)
	{
		String ^token = nullptr;

		if (str != nullptr)
		{
			str = str->Trim();

			if (str->Length > 0)
			{
				int i = 0;
				while (i < str->Length && str[i] != ' ' && str[i] != '\t')
					i++;
				token = str->Substring(0, i);

				if (i != str->Length)
					str = str->Substring(i + 1, str->Length - i - 1);
				else
					str = "";
			}
		}

		return token;
	}

};