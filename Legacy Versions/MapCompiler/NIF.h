
#pragma once

using namespace System;
using namespace System::Collections::Generic;
using namespace WarZoneLib;

ref class NIF
{
public:
	int ID;
	String ^Name, ^Filename;
	double MinAngle, MaxAngle;
};