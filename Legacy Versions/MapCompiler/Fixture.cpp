
#include "Fixture.h"
#include "NIF.h"
#include "Zone.h"
#include "Utility.h"

Matrix Fixture::RotationMatrix::get()
{
	::NIF ^nif = Zone->GetNIFByID(NIF);

	Vector3 axis = Vector3(-Axis3D.X, Axis3D.Y, Axis3D.Z);
	Matrix matrix3d = Matrix::RotationAxis(axis, (float)Angle3D);

	float angle2d = (float)Utility::Clamp(A, nif->MinAngle, nif->MaxAngle);
	angle2d = Utility::ToRadians(angle2d);
	Matrix matrix2d = Matrix::RotationZ(angle2d);

	// TODO: still need to determine order here
	return matrix2d * matrix3d * Matrix::RotationZ((float)Math::PI);
}

Matrix Fixture::TranslationMatrix::get()
{
	return Matrix::Translation(65535.0f - (float)X, (float)Y, (float)Z);
}
