﻿namespace WarClient.Figleaf.Tables
{
    public abstract class FigRecord
    {
        protected FigleafDB DB { get; private set; }
        public FigRecord(FigleafDB db)
        {
            DB = db;
        }
    }
}
