﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WarClient.Figleaf.Tables
{
    public class LightmapTable : FigTable<Lightmap>
    {
         private const int HeaderPosition = 0x84;
        private const int RecordSize = 0x28;

        public LightmapTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var lightmap = new Lightmap(_db, i);

                lightmap.B01 = reader.ReadUInt32();
                lightmap.B02 = reader.ReadUInt32();
                lightmap.B03 = reader.ReadUInt32();
                lightmap.B04 = reader.ReadUInt32();
                lightmap.B05_Pages = reader.ReadUInt32();
                lightmap.SourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                lightmap.B07 = reader.ReadUInt32();
                lightmap.B08 = reader.ReadUInt32();
                lightmap.B09a = reader.ReadUInt32();
                lightmap.B10a = reader.ReadUInt32();

                Records.Add(lightmap);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (int i = 0; i < Records.Count; i++)
            {
                var lightmap = Records[i];
                writer.Write(lightmap.B01);
                writer.Write(lightmap.B02);
                writer.Write(lightmap.B03);
                writer.Write(lightmap.B04);
                writer.Write(lightmap.B05_Pages);
                writer.Write((uint)lightmap.SourceIndex);
                writer.Write(lightmap.B07);
                writer.Write(lightmap.B08);
                writer.Write(lightmap.B09a);
                writer.Write(lightmap.B10a);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public class Lightmap:FigRecord
    {
        public int Index { get; }
        public uint B01{ get; set; }
        public uint B02{ get; set; }
        public uint B03{ get; set; }
        public uint B04{ get; set; }
        public uint B05_Pages{ get; set; }
        public FigStringRef SourceIndex{ get; set; }
        public uint B07{ get; set; }
        public uint B08{ get; set; }
        public uint B09a{ get; set; }
        public uint B10a{ get; set; }
        public Lightmap(FigleafDB db, int index) : base(db) { Index = index; }
    }
}

