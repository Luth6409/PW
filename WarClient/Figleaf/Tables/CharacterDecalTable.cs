﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class CharacterDecalTable : FigTable<CharacterDecal>
    {
        private const int HeaderPosition = 0x48;
        private const int RecordSize = 0x3C;

        public CharacterDecalTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var decal = new CharacterDecal(_db, i);
                decal.Type = (DecalType)reader.ReadUInt32();
                decal.FileID =  reader.ReadInt32();

                decal.AlphaBits = reader.ReadByte();
                decal.TintType = reader.ReadByte();
                decal.Dye1 = reader.ReadByte();
                decal.Dye2 = reader.ReadByte();

                decal.MaskIndex = new FigStringRef(_db, reader.ReadInt32());
                decal.MaskHash = reader.ReadInt64();
                decal.MaskHashB = reader.ReadInt32();
                decal.DiffuseNormalGlowIndex = new FigStringRef(_db, reader.ReadInt32());
                decal.DiffuseNormalHash = reader.ReadInt64();
                decal.DiffuseNormalHashB = reader.ReadInt32();
                decal.TintSpecularIndex = new FigStringRef(_db, reader.ReadInt32());
                decal.TintSpecularHash = reader.ReadInt64();
                decal.TintSpecularHashB = reader.ReadInt32();


                Records.Add(decal);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (int i = 0; i < Records.Count; i++)
            {
                var decal = Records[i];

                writer.Write((uint)decal.Type);
                writer.Write((int)decal.FileID);
                writer.Write(decal.AlphaBits);
                writer.Write(decal.TintType);
                writer.Write(decal.Dye1);
                writer.Write(decal.Dye2);
                writer.Write((uint)decal.MaskIndex);
                writer.Write(decal.MaskHash);
                writer.Write(decal.MaskHashB);
                writer.Write((uint)decal.DiffuseNormalGlowIndex);
                writer.Write(decal.DiffuseNormalHash);
                writer.Write(decal.DiffuseNormalHashB);
                writer.Write((uint)decal.TintSpecularIndex);
                writer.Write(decal.TintSpecularHash);
                writer.Write(decal.TintSpecularHashB);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public enum DecalType:int
    {
        Mask = 1179413857,
        Diffuse = 1179411561,
        Tint = 1179415662,
        Glow = 1179412332,
        Normal = 1179414127,
        Specular = 1179415408
    }

    public class CharacterDecal : FigRecord
    {
        public int Index { get; }
        public DecalType Type{ get; set; }
        public int FileID{ get; set; }

        public FigStringRef MaskIndex{ get; set; }

        public byte AlphaBits{ get; set; }
        public byte TintType{ get; set; }
        public byte Dye1{ get; set; }
        public byte Dye2{ get; set; }

     
        public long MaskHash{ get; set; }
        public int MaskHashB{ get; set; }

        public FigStringRef DiffuseNormalGlowIndex{ get; set; }
        public long DiffuseNormalHash{ get; set; }
        public int DiffuseNormalHashB{ get; set; }

        public FigStringRef TintSpecularIndex{ get; set; }
        public long TintSpecularHash{ get; set; }
        public int TintSpecularHashB{ get; set; }

        public CharacterDecal(FigleafDB db, int index): base(db) { Index = index; }
    }

    public class CharacterDecalRef
    {
        private FigleafDB _db;

        public static implicit operator int(CharacterDecalRef r)
        {
            return r.Index;
        }

        public override string ToString()
        {
            if (Index >= 0 && Index <= _db.TableCharacterDecals.Records.Count)
                return _db.TableCharacterDecals.Records[Index].MaskIndex.ToString() + " [" + Index + "]";
            return Index.ToString();
        }

        public int Index { get; set; }
        public CharacterDecalRef(FigleafDB db, int value)
        {
            _db = db;
            Index = value;
        }
    }
}

