﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class ArtSwitchTable : FigTable<ArtSwitch>
    {
        private const int HeaderPosition = 0x3c;
        private const int RecordSize = 0xA4;

        public ArtSwitchTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var art = new ArtSwitch(_db, i);
                art.SwitchType = (SwitchType)reader.ReadUInt32();
                art.SourceIndex  = new FigStringRef(_db, reader.ReadUInt32());
                art.Unk2 = reader.ReadUInt32();
                art.Unk3 = reader.ReadUInt32();
                art.Unk4 = reader.ReadUInt32();
                art.Other = new FigStringRef(_db, reader.ReadUInt32());
                art.Male = new FigStringRef(_db, reader.ReadUInt32());
                art.Female = new FigStringRef(_db, reader.ReadUInt32());
                art.DwarfOther = new FigStringRef(_db, reader.ReadUInt32());
                art.DwarfMale = new FigStringRef(_db, reader.ReadUInt32());
                art.DwarfFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.HumanOther = new FigStringRef(_db, reader.ReadUInt32());
                art.HumanMale = new FigStringRef(_db, reader.ReadUInt32());
                art.HumanFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosHumanOther = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosHumanMale = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosHumanFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.ElfOther = new FigStringRef(_db, reader.ReadUInt32());
                art.ElfMale = new FigStringRef(_db, reader.ReadUInt32());
                art.ElfFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.DarkElfOther = new FigStringRef(_db, reader.ReadUInt32());
                art.DarkElfMale = new FigStringRef(_db, reader.ReadUInt32());
                art.DarkElfFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.OrcOther = new FigStringRef(_db, reader.ReadUInt32());
                art.OrcMale = new FigStringRef(_db, reader.ReadUInt32());
                art.OrcFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.GoblinOther = new FigStringRef(_db, reader.ReadUInt32());
                art.GoblinMale = new FigStringRef(_db, reader.ReadUInt32());
                art.GoblinFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.BeastmanOther = new FigStringRef(_db, reader.ReadUInt32());
                art.BeastmanMale = new FigStringRef(_db, reader.ReadUInt32());
                art.BeastmanFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.SkavenOther = new FigStringRef(_db, reader.ReadUInt32());
                art.SkavenMale = new FigStringRef(_db, reader.ReadUInt32());
                art.SkavenFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.OgreOther = new FigStringRef(_db, reader.ReadUInt32());
                art.OgreMale = new FigStringRef(_db, reader.ReadUInt32());
                art.OgreFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosWarriorOther = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosWarriorMale = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosWarriorFemale = new FigStringRef(_db, reader.ReadUInt32());
                Records.Add(art);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (int i = 0; i < Records.Count; i++)
            {
                var art = Records[i];

                writer.Write((uint)art.SwitchType);
                writer.Write((uint)art.SourceIndex);
                writer.Write((uint)art.Unk2);
                writer.Write((uint)art.Unk3);
                writer.Write((uint)art.Unk4);
                writer.Write((uint)art.Other);
                writer.Write((uint)art.Male);
                writer.Write((uint)art.Female);
                writer.Write((uint)art.DwarfOther);
                writer.Write((uint)art.DwarfMale);
                writer.Write((uint)art.DwarfFemale);
                writer.Write((uint)art.HumanOther);
                writer.Write((uint)art.HumanMale);
                writer.Write((uint)art.HumanFemale);
                writer.Write((uint)art.ChaosHumanOther);
                writer.Write((uint)art.ChaosHumanMale);
                writer.Write((uint)art.ChaosHumanFemale);
                writer.Write((uint)art.ElfOther);
                writer.Write((uint)art.ElfMale);
                writer.Write((uint)art.ElfFemale);
                writer.Write((uint)art.DarkElfOther);
                writer.Write((uint)art.DarkElfMale);
                writer.Write((uint)art.DarkElfFemale);
                writer.Write((uint)art.OrcOther);
                writer.Write((uint)art.OrcMale);
                writer.Write((uint)art.OrcFemale);
                writer.Write((uint)art.GoblinOther);
                writer.Write((uint)art.GoblinMale);
                writer.Write((uint)art.GoblinFemale);
                writer.Write((uint)art.BeastmanOther);
                writer.Write((uint)art.BeastmanMale);
                writer.Write((uint)art.BeastmanFemale);
                writer.Write((uint)art.SkavenOther);
                writer.Write((uint)art.SkavenMale);
                writer.Write((uint)art.SkavenFemale);
                writer.Write((uint)art.OgreOther);
                writer.Write((uint)art.OgreMale);
                writer.Write((uint)art.OgreFemale);
                writer.Write((uint)art.ChaosWarriorOther);
                writer.Write((uint)art.ChaosWarriorMale);
                writer.Write((uint)art.ChaosWarriorFemale);
            }
            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public enum SwitchType : uint
    {
        Mesh = 0,
        Decal = 1,
        Color = 2
    }

    public class ArtSwitch : FigRecord
    {
        public int Index { get; }
        public SwitchType SwitchType { get; set; }

        public FigStringRef SourceIndex { get; set; }
        public uint Unk2 { get; set; }
        public uint Unk3 { get; set; }
        public uint Unk4 { get; set; }
        public FigStringRef Other { get; set; }
        public FigStringRef Male { get; set; }
        public FigStringRef Female { get; set; }
        public FigStringRef DwarfOther { get; set; }
        public FigStringRef DwarfMale { get; set; }
        public FigStringRef DwarfFemale { get; set; }
        public FigStringRef HumanOther { get; set; }
        public FigStringRef HumanMale { get; set; }
        public FigStringRef HumanFemale { get; set; }
        public FigStringRef ChaosHumanOther { get; set; }
        public FigStringRef ChaosHumanMale { get; set; }
        public FigStringRef ChaosHumanFemale { get; set; }
        public FigStringRef ElfOther { get; set; }
        public FigStringRef ElfMale { get; set; }
        public FigStringRef ElfFemale { get; set; }
        public FigStringRef DarkElfOther { get; set; }
        public FigStringRef DarkElfMale { get; set; }
        public FigStringRef DarkElfFemale { get; set; }
        public FigStringRef OrcOther { get; set; }
        public FigStringRef OrcMale { get; set; }
        public FigStringRef OrcFemale { get; set; }
        public FigStringRef GoblinOther { get; set; }
        public FigStringRef GoblinMale { get; set; }
        public FigStringRef GoblinFemale { get; set; }
        public FigStringRef BeastmanOther { get; set; }
        public FigStringRef BeastmanMale { get; set; }
        public FigStringRef BeastmanFemale { get; set; }
        public FigStringRef SkavenOther { get; set; }
        public FigStringRef SkavenMale { get; set; }
        public FigStringRef SkavenFemale { get; set; }
        public FigStringRef OgreOther { get; set; }
        public FigStringRef OgreMale { get; set; }
        public FigStringRef OgreFemale { get; set; }
        public FigStringRef ChaosWarriorOther { get; set; }
        public FigStringRef ChaosWarriorMale { get; set; }
        public FigStringRef ChaosWarriorFemale { get; set; }

        public ArtSwitch(FigleafDB db, int index) : base(db) { Index = index; }
    }
}
