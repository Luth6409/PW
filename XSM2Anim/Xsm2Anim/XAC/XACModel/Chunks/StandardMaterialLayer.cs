﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{

    public class StandardMaterialLayer
    {
        public string mName;
        public string mTextureType;

        public StandardMaterialLayer(string iName, string iTextureType)
        {
            mName = string.Empty;
            if (!string.IsNullOrEmpty(iName))
            {
                mName = iName;
            }
            mTextureType = string.Empty;
            if (!string.IsNullOrEmpty(iTextureType))
            {
              mTextureType = iTextureType;
            }
        }

        public override string ToString()
        {
            string vName = string.Empty;
            if (!string.IsNullOrEmpty(mName))
            {
                vName = mName;
            }
            string vTextureType = string.Empty;
            if (!string.IsNullOrEmpty(mTextureType))
            {
              vTextureType = mTextureType;
            }
            string vTheString = "XACMatTextureEntry Name: " + vName;
            if (!string.IsNullOrEmpty(vTextureType))
            {
              vTheString += "  TextureType: " + vTextureType;
            }
            return vTheString;
        }
    }

}
