﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class CharacterMeshesTable : FigTable
    {
        public List<CharacterMeshView> Meshes;
        
        public override int RecordSize =>  Marshal.SizeOf(typeof(CharacterMesh));
        public override List<object> Records => Meshes.Select(e => (object)e).ToList();

        public CharacterMeshesTable(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.CharacterMeshes, db, data, reader)
        {
        }

        protected override int LoadInternal()
        {
            Meshes = ReadStructs<CharacterMesh>(_fileData, Offset, (int)EntryCount).Select(e => new CharacterMeshView(e)).ToList();

            int i = 0;
            Meshes.ForEach(e =>
                {
                    e.Index = i;

                    if (e.SourceIndex < _db.TableStrings1.Strings.Count)
                        e.Name = _db.TableStrings1.Strings[(int)e.SourceIndex].Value;
                    else
                        e.Name = "!INALID_INDEX!";

                    i++;
                }
            );

            return RecordSize * (int)EntryCount;
        }

    }

   
}

