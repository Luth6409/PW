﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace WarFigleaf.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Region
    {
        public uint A00;
        public uint SliceCount;
        public int ZoneStartOffset;
        public uint RegionID;
        public int A16;
        public int A20;
        public uint ZoneCount;
        public int ZoneEndOffse;
    }

    public class RegionView
    {
        public Region Region;
        public List<ZoneView> Zones = new List<ZoneView>();
        public List<ZoneSliceView> Slices = new List<ZoneSliceView>();

        public RegionView(Region region)
        {
            Region = region;
        }

        public uint A00 { get => Region.A00; set => Region.A00 = value; }
        public uint SliceCount { get => Region.SliceCount; set => Region.SliceCount = value; }
        public int ZoneStartOffset { get => Region.ZoneStartOffset; set => Region.ZoneStartOffset = value; }
        public uint RegionID { get => Region.RegionID; set => Region.RegionID = value; }
        public int A16 { get => Region.A16; set => Region.A16 = value; }
        public int A20 { get => Region.A20; set => Region.A20 = value; }
        public uint ZoneCount { get => Region.ZoneCount; set => Region.ZoneCount = value; }
        public int ZoneEndOffse { get => Region.ZoneEndOffse; set => Region.ZoneEndOffse = value; }


    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ZoneSlice
    {
        public uint Unk1 { get; set; }
        public uint Unk2 { get; set; }
        public uint Unk3 { get; set; }
        public uint Unk4 { get; set; }
    }
    public class ZoneSliceView
    {
        public ZoneSlice Slice;
        public ZoneSliceView(ZoneSlice lice)
        {
            Slice = lice;
        }

        public uint Unk1 { get => Slice.Unk1; set => Slice.Unk1 = value; }
        public uint Unk2 { get => Slice.Unk2; set => Slice.Unk2 = value; }
        public uint Unk3 { get => Slice.Unk3; set => Slice.Unk3 = value; }
        public uint Unk4 { get => Slice.Unk4; set => Slice.Unk4 = value; }

        public void Save(BinaryWriter dataWriter)
        {
            dataWriter.Write((UInt32)Unk1);
            dataWriter.Write((UInt32)Unk2);
            dataWriter.Write((UInt32)Unk3);
            dataWriter.Write((UInt32)Unk4);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Zone
    {
        public ushort ZoneID;
        public ushort MiniMapID;
        public ushort ZoneXOff;
        public ushort ZoneYOff;
        public ushort Unk1;
        public ushort Unk2;
        public ushort Unk3;
        public ushort Unk4;
        public ushort Unk5;
        public ushort Unk6;
        public ushort Unk7;
        public ushort Unk8;
        public ushort Unk9;
        public ushort Unk10;
        public ushort Unk11;
    }

    public class ZoneView
    {
        public Zone Zone;
        public string Name { get; set; }
        public ZoneView(Zone zone)
        {
            Zone = zone;
        }

        public ushort ZoneID { get => Zone.ZoneID; set => Zone.ZoneID = value; }
        public ushort MiniMapID { get => Zone.MiniMapID; set => Zone.MiniMapID = value; }
        public ushort ZoneXOff { get => Zone.ZoneXOff; set => Zone.ZoneXOff = value; }
        public ushort ZoneYOff { get => Zone.ZoneYOff; set => Zone.ZoneYOff = value; }
        public ushort Unk1 { get => Zone.Unk1; set => Zone.Unk1 = value; }
        public ushort Unk2 { get => Zone.Unk2; set => Zone.Unk2 = value; }
        public ushort Unk3 { get => Zone.Unk3; set => Zone.Unk3 = value; }
        public ushort Unk4 { get => Zone.Unk4; set => Zone.Unk4 = value; }
        public ushort Unk5 { get => Zone.Unk5; set => Zone.Unk5 = value; }
        public ushort Unk6 { get => Zone.Unk6; set => Zone.Unk6 = value; }
        public ushort Unk7 { get => Zone.Unk7; set => Zone.Unk7 = value; }
        public ushort Unk8 { get => Zone.Unk8; set => Zone.Unk8 = value; }
        public ushort Unk9 { get => Zone.Unk9; set => Zone.Unk9 = value; }
        public ushort Unk10 { get => Zone.Unk10; set => Zone.Unk10 = value; }
        public ushort Unk11 { get => Zone.Unk11; set => Zone.Unk11 = value; }

        public void Save(BinaryWriter dataWriter)
        {
            dataWriter.Write((UInt16)ZoneID);
            dataWriter.Write((UInt16)MiniMapID);
            dataWriter.Write((UInt16)ZoneXOff);
            dataWriter.Write((UInt16)ZoneYOff);
            dataWriter.Write((UInt16)Unk1);
            dataWriter.Write((UInt16)Unk2);
            dataWriter.Write((UInt16)Unk3);
            dataWriter.Write((UInt16)Unk4);
            dataWriter.Write((UInt16)Unk5);
            dataWriter.Write((UInt16)Unk6);
            dataWriter.Write((UInt16)Unk7);
            dataWriter.Write((UInt16)Unk8);
            dataWriter.Write((UInt16)Unk9);
            dataWriter.Write((UInt16)Unk10);
            dataWriter.Write((UInt16)Unk11);
        }
    }
}
