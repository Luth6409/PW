﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using WarFigleaf.Tables;

namespace WarFigleaf.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FigString2
    {
        public int Index;
        public string Value;
    }

    public class FigString2View
    {
        public FigString2 FigString2;
        public FigString2View(FigString2 figString2)
        {
            FigString2 = figString2;
        }

        public int Index { get => FigString2.Index; set => FigString2.Index = value; }
        public String Value { get => FigString2.Value; set => FigString2.Value = value; }


        public override string ToString()
        {
            return Value;
        }
    }
}
