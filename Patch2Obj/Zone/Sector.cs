﻿using System;
using System.IO;

namespace UnexpectedBytes.War.Terrain
{
    public class Sector
    {
        public Int32 ScaleFactor { get; set; }
        public Int32 OffsetFactor { get; set; }

        public Int32 SizeX { get; set; }
        public Int32 SizeY { get; set; }

        public Sector() { }

        public Sector(String zones_path, Int32 zone_id) => Load(zones_path, zone_id);

        public void Load(String zones_path, Int32 zone_id)
        {
            var sector = Path.Combine(zones_path, $"zone{zone_id:D3}", $"sector.dat");
            if (File.Exists(sector))
            {
                System.Console.WriteLine($"[!] Processing {sector}.");

                var text = File.ReadAllText(sector);
                var lines = text.Split((Char)0x0A);

                foreach (var line in lines)
                {
                    if (line.StartsWith("scalefactor=", StringComparison.InvariantCultureIgnoreCase))
                    {
                        ScaleFactor = Int32.Parse(line.Split('=')[1]);
                    }
                    else if (line.StartsWith("offsetfactor=", StringComparison.InvariantCultureIgnoreCase))
                    {
                        OffsetFactor = Int32.Parse(line.Split('=')[1]);
                    }
                    else if (line.StartsWith("sizex=", StringComparison.InvariantCultureIgnoreCase))
                    {
                        SizeX = Int32.Parse(line.Split('=')[1]);
                    }
                    else if (line.StartsWith("sizey=", StringComparison.InvariantCultureIgnoreCase))
                    {
                        SizeY = Int32.Parse(line.Split('=')[1]);
                    }
                }
            }
        }
    }
}
