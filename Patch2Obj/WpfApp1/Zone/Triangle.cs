﻿using System.Drawing;
using System.IO;
using System.Numerics;

namespace UnexpectedBytes.War.Terrain
{
    public class Triangle
    {
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }

        public Triangle()
        {
            A = 0;
            B = 0;
            C = 0;
        }
    }
}
