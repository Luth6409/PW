﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
  [Flags]
  public enum MypState : Int32
    {
    None = (1 << 0x00),
    Idle = (1 << 0x01),
    Creating = (1 << 0x02),
    Created = (1 << 0x03),
    Opening = (1 << 0x04),
    Opened = (1 << 0x05),
    Loading = (1 << 0x06),
    Loaded = (1 << 0x07),
    Reading = (1 << 0x08),
    Read = (1 << 0x09),
    Decompressing = (1 << 0x0a),
    Decompressed = (1 << 0x0b),
    Compressing = (1 << 0x0c),
    Compressed = (1 << 0x0d),
    Writing = (1 << 0x0e),
    Written = (1 << 0x0f),
    Unloading = (1 << 0x10),
    Unloaded = (1 << 0x11),
    Closing = (1 << 0x12),
    Closed = (1 << 0x13),
    Searching = (1 << 0x14),
    SearchSucceeded = (1 << 0x15),
    SearchFailed = (1 << 0x16),
    ErrorRecoverable = (1 << 0x17),
    ErrorFatal = (1 << 0x18),
    Updating = (1 << 0x19),
    Updated = (1 << 0x1a),
    Deleting = (1 << 0x1b),
    Deleted = (1 << 0x1c),
    Extracting = (1 << 0x1d),
    Extracted = (1 << 0x1e),
    Patching = (1 << 0x1f),
    Patched = (1 << 0x20),
    Packing = (1 << 0x21),
    Packed = (1 << 0x22),
    Unpacking = (1 << 0x23),
    Unpacked = (1 << 0x24)
  }
}