
[Server] packet : (0x1B) F_REQ_CAMPAIGN_STATUS  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 1B 00 2C 19 00 00 12 E6 F2 00 0D 90 00    |............... |
-------------------------------------------------------------------
[Client] packet : (0xD2) F_INTERACT  Size = 24 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 11 97 00 36 00 00 00 D2 00 00 08 BE 00 00 |.....6..........|
|00 00 00 00 00 00 00 00                         |........        |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 60 A0 2C 19 12 1E 2C 19 01 01 00 |.....`.,...,....|
|00 00 00 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 60 A0 2C 19 12 1E 2C 19 02 01 00 |.....`.,...,....|
|00 00 00 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x7E) F_SET_ABILITY_TIMER  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 7E 00 01 01 00 00 00 00 00 00 00 00 00    |............... |
-------------------------------------------------------------------
[Server] packet : (0xE9) F_INTERACT_RESPONSE  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0A E9 00 08 BE 00 00 01 04 00 70 00          |.............   |
-------------------------------------------------------------------
[Client] packet : (0x16) F_REQUEST_INIT_OBJECT  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 04 11 9B 00 36 00 00 00 16 24 17 00 00 A7 6B |.....6....$....k|                                                
-------------------------------------------------------------------
[Client] packet : (0x0B) F_PING  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 11 9D 00 36 00 00 00 0B 00 18 86 2D 8F 78 |.....6.......-.x|
|C5 95 27 46 AB 6A 00 00 00 00 00 00 00 00 02 E8 |..'F.j..........|                                                
-------------------------------------------------------------------
[Server] packet : (0x81) S_PONG  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 81 00 18 86 2D 52 84 B1 D1 00 00 00 00 00 |......-R........|
|00 11 9E 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 60 A0 2C 19 12 1E 2C 19 01 01 00 |.....`.,...,....|
|00 00 00 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 60 A0 2C 19 12 1E 2C 19 02 01 00 |.....`.,...,....|
|00 00 00 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0xD2) F_INTERACT  Size = 24 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 11 A1 00 36 00 00 00 D2 00 00 08 BE 00 19 |.....6..........|
|00 00 00 00 00 00 00 00                         |........        |
-------------------------------------------------------------------
[Server] packet : (0x1B) F_REQ_CAMPAIGN_STATUS  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 1B 00 13 41 00 00 12 D9 F0 00 0D 79 00    |............... |
-------------------------------------------------------------------
[Server] packet : (0xE9) F_INTERACT_RESPONSE  Size = 8 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 05 E9 12 08 BE 00 00                         |........        |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 60 A0 2C 19 12 1E 2C 19 01 01 00 |.....`.,...,....|
|00 00 00 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 60 A0 2C 19 12 1E 2C 19 02 01 00 |.....`.,...,....|
|00 00 00 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x1B) F_REQ_CAMPAIGN_STATUS  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 1B 00 2C 19 00 00 12 DF 83 00 0D 8E 00    |............... |
-------------------------------------------------------------------
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 11 A6 00 36 00 00 00 01 00 18 8F 2E 20 07 |.....6........ .|
|2A 68 90 38 40 04 00 23 7D F4 08 18 DE 7A 0B 5D |*h.8@..#}....z.]|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 18 8F 2E B7 D6 8F CA 00 00 00 00 00 |................|
|23 7D F4 08 18 DE 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x86) F_MAIL  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 04 11 A8 00 36 FF FF 00 86 00 00 00 00 00 00 |.....6..........|                                                
-------------------------------------------------------------------
[Server] packet : (0x7E) F_SET_ABILITY_TIMER  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 7E 00 F5 00 00 00 00 00 00 00 00 00 00    |............... |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 60 A0 2C 19 12 1E 2C 19 01 01 00 |.....`.,...,....|
|00 00 00 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 60 A0 2C 19 12 1E 2C 19 02 01 00 |.....`.,...,....|
|00 00 00 00 00 00 00                            |.......         |
-------------------------------------------------------------------