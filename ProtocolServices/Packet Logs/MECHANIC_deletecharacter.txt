
[Client] packet : (0x0B) F_PING  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 B2 47 00 01 00 00 00 0B 00 B3 B2 DB EB 3F |...G...........?|
|2E F8 9E 7B 5B 56 00 00 00 00 00 00 00 00 52 B3 |...{[V........R.|                                                
-------------------------------------------------------------------
[Server] packet : (0x81) S_PONG  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 81 00 B3 B2 DB 52 80 E1 50 00 00 00 00 00 |.......R..P.....|
|00 B2 48 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x0B) F_PING  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 B2 48 00 01 00 00 00 0B 00 B3 C5 EB F7 3F |...H...........?|
|FB FC BC 38 E8 2C 00 00 00 00 00 00 00 00 1D 00 |...8.,..........|                                                
-------------------------------------------------------------------
[Server] packet : (0x81) S_PONG  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 81 00 B3 C5 EB 52 80 E1 54 00 00 00 00 00 |.......R..T.....|
|00 B2 49 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x0B) F_PING  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 B2 49 00 01 00 00 00 0B 00 B3 D9 79 D9 50 |...I.........y.P|
|C1 96 00 C7 8F 50 00 00 00 00 00 00 00 00 C6 05 |.....P..........|                                                
-------------------------------------------------------------------
[Server] packet : (0x81) S_PONG  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 81 00 B3 D9 79 52 80 E1 5A 00 00 00 00 00 |......yR..Z.....|
|00 B2 4A 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x92) F_DELETE_CHARACTER  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 01 B2 4A 00 01 FF FE 00 92 0C 00 00          |.............   |
-------------------------------------------------------------------
[Server] packet : (0x58) F_SEND_CHARACTER_RESPONSE  Size = 27 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 18 58 79 61 6C 69 73 6B 61 68 5F 45 55 52 00 |..Xyaliskah_EUR.|
|00 00 00 00 00 00 00 00 00 00 00                |...........     |
-------------------------------------------------------------------
[Client] packet : (0x0B) F_PING  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 B2 4B 00 01 00 00 00 0B 00 B3 ED 02 FD FF |...K............|
|3B F4 8A BB D2 4A 00 00 00 00 00 00 00 00 2D 44 |;....J........-D|                                                
-------------------------------------------------------------------
[Server] packet : (0x81) S_PONG  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 81 00 B3 ED 02 52 80 E1 5F 00 00 00 00 00 |.......R.._.....|
|00 B2 4C 00 00 00 00                            |.......         |
-------------------------------------------------------------------