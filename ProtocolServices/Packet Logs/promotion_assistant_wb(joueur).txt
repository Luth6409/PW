
[Client] packet : (0x5A) F_PING_DATAGRAM  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 01 66 8E 00 DD FF FF 00 5A 00 B9 C1          |.............   |
-------------------------------------------------------------------
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 66 91 00 DD 00 00 00 01 00 65 4A EF 00 23 |..f........eJ..#|
|39 B0 08 18 C0 4A 20 07 6A FC 90 38 55 EC 00 00 |9....J .j..8U...|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 65 4A EF AA EB AE 4F 00 00 00 00 20 |....eJ....O.... |
|07 6A FC 90 38 55 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x16) F_REQUEST_INIT_OBJECT  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 04 66 98 00 DD 00 00 00 16 34 E0 00 00 72 74 |..f.......4...rt|                                                
-------------------------------------------------------------------
[Server] packet : (0x72) F_CREATE_MONSTER  Size = 108 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 69 72 34 E0 00 EB 00 A4 24 20 00 0F D9 50 00 |.ir4.....$ ...P.|
|10 40 02 FF FC 04 3C 32 07 61 00 00 00 00 00 00 |.@....<2.a......|
|00 00 00 03 E8 00 00 00 00 89 74 00 05 00 00 01 |..........t.....|
|19 00 47 6F 6C 64 65 6E 77 69 6E 67 20 45 61 67 |..Goldenwing Eag|
|6C 65 5E 6D 00 01 0A 00 00 00 1A 05 00 34 E0 59 |le^m.........4.Y|
|50 C0 02 24 20 64 21 C8 00 00 00 00 00 EB 00 81 |P..$ d!.........|
|14 59 ED C0 1C 24 C8 34 E0 00 00 00             |............    |
-------------------------------------------------------------------
[Client] packet : (0x16) F_REQUEST_INIT_OBJECT  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 04 66 A0 00 DD 00 00 00 16 11 6A 00 00 DA B3 |..f........j....|                                                
-------------------------------------------------------------------
[Server] packet : (0x72) F_CREATE_MONSTER  Size = 108 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 69 72 11 6A 00 EB 0C FA 2B E6 00 0F D0 22 00 |.ir.j....+....".|
|10 41 AF 00 00 04 3C 32 07 61 00 00 00 00 00 00 |.A....<2.a......|
|00 00 00 03 E8 00 00 00 00 89 66 00 08 00 00 01 |..........f.....|
|19 00 47 6F 6C 64 65 6E 77 69 6E 67 20 46 6C 69 |..Goldenwing Fli|
|65 72 5E 6D 00 01 0A 00 00 00 1A 05 00 11 6A 50 |er^m..........jP|
|22 C1 AF 2B E6 64 21 C8 00 00 00 00 00 EB 00 00 |"..+.d!.........|
|23 51 17 C2 E6 2B C8 11 6A 00 00 00             |............    |
-------------------------------------------------------------------
[Server] packet : (0x51) F_ANIMATION  Size = 9 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 06 51 01 3C 00 E5 00 00                      |.........       |
-------------------------------------------------------------------
[Server] packet : (0xD8) F_GROUP_STATUS  Size = 17 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0E D8 00 00 00 00 0E 02 EA A1 86 01 CC DF 96 |................|
|00                                              |.               |
-------------------------------------------------------------------
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 66 A8 00 DD 00 00 00 01 00 65 72 5A 20 07 |..f........erZ .|
|2A 68 90 38 40 04 00 23 7B F0 08 18 F6 5B 3B B3 |*h.8@..#{....[;.|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 65 72 5A AA EB D5 A3 00 00 00 00 00 |....erZ.........|
|23 7B F0 08 18 F6 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0xD8) F_GROUP_STATUS  Size = 20 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 11 D8 00 00 00 00 0E 03 9C E4 6C EA A1 86 01 |...........l....|
|CC DF 96 00                                     |....            |
-------------------------------------------------------------------
[Server] packet : (0x6F) F_LOCALIZED_STRING  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0A 6F 00 00 00 00 01 F0 00 00 00 00          |.............   |
-------------------------------------------------------------------
[Client] packet : (0x5A) F_PING_DATAGRAM  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 01 66 B0 00 DD FF FF 00 5A 00 87 D1          |.............   |
-------------------------------------------------------------------