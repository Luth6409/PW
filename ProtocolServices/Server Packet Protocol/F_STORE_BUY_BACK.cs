using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_STORE_BUY_BACK, FrameType.Game)]
    public class F_STORE_BUY_BACK : Frame
    {
        public F_STORE_BUY_BACK() : base((int)GameOp.F_STORE_BUY_BACK)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
