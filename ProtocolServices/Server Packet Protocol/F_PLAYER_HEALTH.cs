using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_HEALTH, FrameType.Game)]
    public class F_PLAYER_HEALTH : Frame
    {
        private float Health;
        private float MaxWounds;
        private float ActionPoints;
        private float MaxActionPoints;
        private ushort Morale;

        public F_PLAYER_HEALTH() : base((int)GameOp.F_PLAYER_HEALTH)
        {
        }

        public static F_PLAYER_HEALTH Create(float health, float maxWounds, float actionPoints, float maxActionPoints, ushort morale)
        {
            return new F_PLAYER_HEALTH()
            {
                MaxWounds = maxWounds,
                Health = health,
                ActionPoints = actionPoints,
                MaxActionPoints = maxActionPoints,
                Morale = morale,
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt32((uint)Health);
            WriteUInt32((uint)MaxWounds);
            WriteUInt16((ushort)ActionPoints); 
            WriteUInt16((ushort)MaxActionPoints);
            WriteUInt16((ushort)(Morale)); 
            WriteUInt16(0x0E10);
        }

    }
}
