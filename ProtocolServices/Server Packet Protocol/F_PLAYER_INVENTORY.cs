using System.Collections.Generic;
using System.Linq;
using WarServer.Game.Entities;
using WarShared;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_INVENTORY, FrameType.Game)]
    public class F_PLAYER_INVENTORY : Frame
    {
        private ushort ObjectID;
        private EntityDataBase Entity;
        private List<PlayerItemData> Inventory;

        public F_PLAYER_INVENTORY() : base((int)GameOp.F_PLAYER_INVENTORY)
        {
        }

        public static F_PLAYER_INVENTORY Create(Entity entity)
        {
            return new F_PLAYER_INVENTORY()
            {
                Inventory = entity.Inventory.Values.ToList(),
                Entity = entity.Data,
                ObjectID = entity.ObjectID
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt16((ushort)ObjectID);
            WriteByte((byte)Entity.WeaponStance); //weapon stance

            var items = Inventory.Where(e => e.SlotIndex1 >= 10 && e.SlotIndex1 <= 28).ToList();

            WriteByte((byte)items.Count);
            byte flags = (byte)InventoryItemType.MODEL_ONLY;
            ushort modelID = 0;

            foreach (var c in items)
            {
                var item = c;

                if (!(item.SlotIndex1 >= 10 && item.SlotIndex1 <= 28))
                    continue;


                modelID = (ushort)item.GetModelID();

                if (c.SlotIndex1 == (ushort)InventorySlotType.HELM && !Entity.ShowHelm)
                    item = new PlayerItemData();

                if (item.SlotIndex1 == 10)
                    item.Type = InventoryItemType.MODEL_ONLY;
                else if (c.SlotIndex1 == (ushort)InventorySlotType.BACK)
                {
                    if (Entity.ShowCloak && Entity.ShowHeraldry)
                        item.Type = InventoryItemType.CLOAK;
                    else
                    {
                        item.Type = InventoryItemType.MODEL_ONLY;
                        if (!Entity.ShowCloak)
                            modelID = 0;
                    }


                }
                else if (c.SlotIndex1 == (ushort)InventorySlotType.STANDARD)
                {
                    if (Entity.ShowCloak && Entity.ShowHeraldry)
                        item.Type = InventoryItemType.CLOAK;
                    else
                    {
                        item = new PlayerItemData();
                        item.Type = InventoryItemType.MODEL_ONLY;
                        item.SlotIndex1 = c.SlotIndex1;
                    }


                }
                else if (item.SlotIndex1 == (ushort)InventorySlotType.TROPY1 || item.SlotIndex1 == (ushort)InventorySlotType.TROPY2 || item.SlotIndex1 == (ushort)InventorySlotType.TROPY3 ||
                    item.SlotIndex1 == (ushort)InventorySlotType.TROPY4 || item.SlotIndex1 == (ushort)InventorySlotType.TROPY5)
                    item.Type = InventoryItemType.TROPY;
                else if (item.Dye1 != 0 || item.Dye2 != 0)
                    item.Type = InventoryItemType.MODEL_WITH_DYE;

                flags = (byte)item.Type;

                if (item.Dye1 > 255)
                    flags |= 0x40;
                if (item.Dye2 > 255)
                    flags |= 0x80;
                if (item.ApperanceItem != null && item.ApperanceItem.ModelID != 0)
                    flags |= 32;

                WriteByte(flags);
                WriteByte((byte)c.SlotIndex1);

                if (item.Type == InventoryItemType.MODEL_ONLY)
                {
                    WriteUInt16((ushort)modelID);
                    if (item.ApperanceItem != null && item.ApperanceItem.ModelID != 0)
                        WriteUInt16((ushort)item.ApperanceItem.ModelID);
                }
                else if (item.Type == InventoryItemType.MODEL_WITH_DYE)
                {
                    WriteUInt16((ushort)item.GetModelID());
                    if (item.ApperanceItem != null && item.ApperanceItem.ModelID != 0)
                        WriteUInt16((ushort)item.ApperanceItem.ModelID);

                    if (item.Dye1 > 255)
                        WriteUInt16((ushort)item.Dye1);
                    else
                        WriteByte((byte)item.Dye1);

                    if (item.Dye2 > 255)
                        WriteUInt16((ushort)item.Dye2);
                    else
                        WriteByte((byte)item.Dye2);
                }
                else if (item.Type == InventoryItemType.TROPY)
                {
                    WriteUInt16((ushort)item.GetModelID());
                    if (item.ApperanceItem != null && item.ApperanceItem.ModelID != 0)
                        WriteUInt16((ushort)item.ApperanceItem.ModelID);

                    WriteByte(item.TrophySlot);
                    WriteByte(item.TrophyData);

                }
                else if (item.Type == InventoryItemType.CLOAK)
                {

                    if (c.SlotIndex1 == (ushort)InventorySlotType.BACK)
                    {

                        WriteUInt16((ushort)item.GetModelID());
                        if (item.ApperanceItem != null && item.ApperanceItem.ModelID != 0)
                            WriteUInt16((ushort)item.ApperanceItem.ModelID);

                        WriteBool(true);

                        WriteUInt16((ushort)item.HeraldryEmblemID);
                        WriteUInt16((ushort)item.HeraldryPattternID);
                        WriteByte((byte)item.HeraldryColorID1);
                        WriteByte((byte)item.HeraldryColorID2);
                        WriteByte((byte)item.HeraldryShapeIndex);
                        WriteByte((byte)item.HeraldryUnk1);
                        WriteByte((byte)item.HeraldryUnk2);
                    }
                    else if (c.SlotIndex1 == (ushort)InventorySlotType.STANDARD)
                    {

                        WriteUInt16((ushort)item.GetModelID());
                        if (item.ApperanceItem != null && item.ApperanceItem.ModelID != 0)
                            WriteUInt16((ushort)item.ApperanceItem.ModelID);

                        WriteBool(true);

                        WriteUInt16((ushort)item.HeraldryEmblemID);
                        WriteUInt16((ushort)item.HeraldryPattternID);
                        WriteByte((byte)item.HeraldryColorID1);
                        WriteByte((byte)item.HeraldryColorID2);
                        WriteByte((byte)item.HeraldryShapeIndex);
                        WriteByte((byte)item.HeraldryUnk1);
                        WriteByte((byte)item.HeraldryUnk2);
                    }
                }
            }

            WriteByte(0);
        }

    }
}
