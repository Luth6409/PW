using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_GUILD_COMMAND, FrameType.Game)]
    public class F_GUILD_COMMAND : Frame
    {
        public F_GUILD_COMMAND() : base((int)GameOp.F_GUILD_COMMAND)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
