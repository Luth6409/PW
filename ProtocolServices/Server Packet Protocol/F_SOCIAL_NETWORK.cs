using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SOCIAL_NETWORK, FrameType.Game)]
    public class F_SOCIAL_NETWORK : Frame
    {
        public F_SOCIAL_NETWORK() : base((int)GameOp.F_SOCIAL_NETWORK)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
