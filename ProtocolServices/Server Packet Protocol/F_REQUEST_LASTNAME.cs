using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REQUEST_LASTNAME, FrameType.Game)]
    public class F_REQUEST_LASTNAME : Frame
    {
        public F_REQUEST_LASTNAME() : base((int)GameOp.F_REQUEST_LASTNAME)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
