using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CRASH_PACKET, FrameType.Game)]
    public class F_CRASH_PACKET : Frame
    {
        public F_CRASH_PACKET() : base((int)GameOp.F_CRASH_PACKET)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
