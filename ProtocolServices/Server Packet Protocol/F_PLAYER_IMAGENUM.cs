using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_IMAGENUM, FrameType.Game)]
    public class F_PLAYER_IMAGENUM : Frame
    {
        private ushort ObjectID;
        public ushort MonsterID;

        public static F_PLAYER_IMAGENUM Create(ushort objectID, ushort monsterID)
        {
            return new F_PLAYER_IMAGENUM()
            {
                ObjectID = objectID,
                MonsterID = monsterID
            };
        }

        public F_PLAYER_IMAGENUM() : base((int)GameOp.F_PLAYER_IMAGENUM)
        {
        }
		
		protected override void SerializeInternal()
        {
            WriteUInt16(ObjectID);
            WriteUInt16(MonsterID);
            Fill(0, 18);
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
