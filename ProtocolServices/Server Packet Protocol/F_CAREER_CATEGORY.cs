using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CAREER_CATEGORY, FrameType.Game)]
    public class F_CAREER_CATEGORY : Frame
    {
        private CategoryData Category;

        public F_CAREER_CATEGORY() : base((int)GameOp.F_CAREER_CATEGORY)
        {
        }

        public static F_CAREER_CATEGORY Create(CategoryData category)
        {
            return new F_CAREER_CATEGORY()
            {
                Category = category
            };
        }

        protected override void SerializeInternal()
        {
            WriteByte((byte)Category.CategoryIndex);
            WriteByte(1);
            WriteUInt32(0);

            WriteUInt16(0);
            WriteUInt16(0);
            WriteUInt16(0);
            WriteUInt16(0);
            WriteUInt16(0);

            WritePascalString(Category.Name);
            WriteUInt16((ushort)Category.Packages.Count);
            for (int i = 0; i < Category.Packages.Count; i++)
            {
                WriteUInt16((ushort)(i + 1));
            }

            WriteByte(0);
            WriteByte(0);
            WriteByte(0);
        }

    }
}
