﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;

namespace WarAdmin.Packets
{
    public class F_TOK_ENTRY_UPDATE : MythicPacket
    {
        public ushort A05_Unk { get; set; }
        public ushort A07_Unk { get; set; }
        public bool A09_Unk { get; set; }
        public bool A10_Unk { get; set; }

        public F_TOK_ENTRY_UPDATE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_TOK_ENTRY_UPDATE, ms, false)
        {
        }
        public override void Load(MemoryStream ms)
        {
            ms.Position = 5;

            A05_Unk = FrameUtil.GetUint16(ms);
            A07_Unk = FrameUtil.GetUint16(ms);

            A09_Unk = FrameUtil.GetUint8(ms) > 0;
            A10_Unk = FrameUtil.GetUint8(ms) > 0;

        }
    }
}
