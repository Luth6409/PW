﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_ITEM_SET_DATA : MythicPacket
    {
        public uint SetID { get; set; }
        public string Name { get; set; }
        public byte Level { get; set; }
        public byte PieceCount { get; set; }
        public byte BonusCount { get; set; }
        public List<SetPiece> Set { get; set; }
        public List<BonusInfo> Bonuses { get; set; }

        public class BonusInfo
        {
            public byte ID { get; set; }
            public byte StatIndex { get; set; }
            public ushort Value { get; set; }
            public byte Unk1 { get; set; }
            public ushort ProcID { get; set; }

            public void DeSerialize(MemoryStream ms)
            {
                ID = FrameUtil.GetUint8(ms);
                if (ID < 40)
                {
                    StatIndex = FrameUtil.GetUint8(ms);
                    Value = FrameUtil.GetUint16(ms);
                    Unk1 = FrameUtil.GetUint8(ms);
                }
                else
                {
                    ProcID = FrameUtil.GetUint16(ms);
                }
            }
        }

        public class SetPiece
        {
            public uint ID { get; set; }
            public string Name { get; set; }

            public void DeSerialize(MemoryStream ms)
            {
                ID = FrameUtil.GetUint32(ms);
                Name = FrameUtil.GetPascalString(ms);
            }
          
        }
        public F_ITEM_SET_DATA()
        {
        }

        public F_ITEM_SET_DATA(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_ITEM_SET_DATA, ms, true)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            SetID = FrameUtil.GetUint32(ms);
            Name = FrameUtil.GetPascalString(ms);
            Level = FrameUtil.GetUint8(ms);
            PieceCount = FrameUtil.GetUint8(ms);
            Set = new List<SetPiece>();
            for (int i = 0; i < PieceCount; i++)
            {
                SetPiece set = new SetPiece();
                set.DeSerialize(ms);
                Set.Add(set);
            }

            Bonuses = new List<BonusInfo>();
            BonusCount = FrameUtil.GetUint8(ms);
            for (int i = 0; i < BonusCount; i++)
            {
                BonusInfo bonus = new BonusInfo();
                bonus.DeSerialize(ms);
                Bonuses.Add(bonus);
            }
        }
    }
}
