﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_OBJECT_EFFECT_STATE : MythicPacket
    {
        public UInt16 ObjectID { get; set; }
        public byte Unk1 { get; set; }
        public byte EffectID { get; set; }
        public byte Enabled { get; set; }
        public byte Unk2 { get; set; }

        public F_OBJECT_EFFECT_STATE()
        {
        }
        public F_OBJECT_EFFECT_STATE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_OBJECT_EFFECT_STATE, ms, true)
        {
        }
    }
}
