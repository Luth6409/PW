﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_GRAPHICAL_REVISION:MythicPacket 
    {
        public ushort A03_ObjectID { get; set; }
        public byte A05_RevisionType { get; set; }
        public byte A06_Unk { get; set; }

        public F_GRAPHICAL_REVISION(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_GRAPHICAL_REVISION, ms, true)
        {
        }
    }
}
