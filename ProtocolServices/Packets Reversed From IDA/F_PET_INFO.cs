﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_PET_INFO : MythicPacket
    {
        public class PetAbility
        {
            public ushort AbilityID { get; set; }
            public byte Unk { get; set; }
            public string AbilityName { get; set; }
            public override string ToString()
            {
                return AbilityName;
            }
        }

        public ushort A03_ObjectID { get; set; }
        public ushort A05_TargetID { get; set; }
        public ushort A07_Count { get; set; } //count
        public byte A09_Unk { get; set; } //followmode
        public byte A10_Unk { get; set; } //followmode
        public ushort A11_Type { get; set; }//state
        public string AbilityNames { get; set; }
        public List<PetAbility> Abilities { get; set; }

        public F_PET_INFO(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PET_INFO, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            A03_ObjectID = FrameUtil.GetUint16(ms);
            A05_TargetID = FrameUtil.GetUint16(ms);
            A07_Count = FrameUtil.GetUint16(ms);
            A09_Unk = FrameUtil.GetUint8(ms);
            A10_Unk = FrameUtil.GetUint8(ms);
            A11_Type = FrameUtil.GetUint8(ms);

            if (A11_Type != 4)
            {
                Abilities = new List<PetAbility>();
                string names = "";
                for (int i = 0; i < A07_Count; i++)
                {
                    var ability = new PetAbility()
                    {
                        AbilityID = FrameUtil.GetUint16(ms),
                        Unk = FrameUtil.GetUint8(ms)

                    };
                    ability.AbilityName = frmMain.Abilities[ability.AbilityID].Name;
                    Abilities.Add(ability);
                    names += ability.AbilityName + " | ";
                }
                AbilityNames = names;
            }
        }


    }
}
