﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Vector3F = FrameWork.Vector3F;

namespace FrameWork
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    [TypeConverter(typeof(Vector3FConverter))]
    public class Vector3F : ICloneable
    {
        #region Private fields
        private float _x;
        private float _y;
        private float _z;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3F"/> class with the coordinates set to 0.0f.
        /// </summary>
        public Vector3F() : this(Zero)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3F"/> class with the specified coordinates.
        /// </summary>
        /// <param name="x">The X coordinate.</param>
        /// <param name="y">The Y coordinate.</param>
        /// <param name="z">The Z coordinate.</param>
        public Vector3F(int x, int y, int z)
        {
            _x = (float)x;
            _y = (float)y;
            _z = (float)z;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3F"/> class with the specified coordinates.
        /// </summary>
        /// <param name="x">The X coordinate.</param>
        /// <param name="y">The Y coordinate.</param>
        /// <param name="z">The Z coordinate.</param>
        public Vector3F(float x, float y, float z)
        {
            _x = x;
            _y = y;
            _z = z;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3F"/> class with the specified coordinates.
        /// </summary>
        /// <param name="coordinates">An array of 3 float coordinates.</param>
        public Vector3F(float[] coordinates)
        {
            Debug.Assert(coordinates != null);
            Debug.Assert(coordinates.Length == 3);

            _x = coordinates[0];
            _y = coordinates[1];
            _z = coordinates[2];
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3F"/> class with the specified coordinates.
        /// </summary>
        /// <param name="coordinates">A list of 3 float coordinates.</param>
        public Vector3F(List<float> coordinates)
        {
            Debug.Assert(coordinates != null);
            Debug.Assert(coordinates.Count == 3);

            _x = coordinates[0];
            _y = coordinates[1];
            _z = coordinates[2];
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3F"/> class using coordinates from a given <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/> to copy coordinates from.</param>
        public Vector3F(Vector3F vector) : this(vector.X, vector.Y, vector.Z)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3F"/> class using coordinates from a given <see cref="Vector2F"/> instance.
        /// </summary>
        /// <param name="other">A <see cref="Vector2F"/> to copy coordinates from.</param>
        public Vector3F(Vector2F vector) : this(vector.X, vector.Y, 0.0f)
        {
        }
        #endregion

        #region Constants
        /// <summary>
        /// 3-Dimentional single-precision floating point zero vector.
        /// </summary>
        public static readonly Vector3F Zero = new Vector3F(0.0f, 0.0f, 0.0f);
        /// <summary>
        /// 3-Dimentional single-precision floating point X-Axis vector.
        /// </summary>
        public static readonly Vector3F XAxis = new Vector3F(1.0f, 0.0f, 0.0f);
        /// <summary>
        /// 3-Dimentional single-precision floating point Y-Axis vector.
        /// </summary>
        public static readonly Vector3F YAxis = new Vector3F(0.0f, 1.0f, 0.0f);
        /// <summary>
        /// 3-Dimentional single-precision floating point Y-Axis vector.
        /// </summary>
        public static readonly Vector3F ZAxis = new Vector3F(0.0f, 0.0f, 1.0f);
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the x-coordinate of this vector.
        /// </summary>
        /// <value>The x-coordinate.</value>
        public float X { get => _x; set => _x = value; }
        /// <summary>
        /// Gets or sets the y-coordinate of this vector.
        /// </summary>
        /// <value>The y-coordinate.</value>
        public float Y { get => _y; set => _y = value; }
        /// <summary>
        /// Gets or sets the z-coordinate of this vector.
        /// </summary>
        /// <value>The z-coordinate.</value>
        public float Z { get => _z; set => _z = value; }
        #endregion

        #region ICloneable members
        /// <summary>
        /// Creates an exact copy of this <see cref="Vector3F"/> object.
        /// </summary>
        /// <returns>The <see cref="Vector3F"/> object this method creates, cast as an object.</returns>
        object ICloneable.Clone()
        {
            return new Vector3F(this);
        }
        /// <summary>
        /// Creates an exact copy of this <see cref="Vector3F"/> object.
        /// </summary>
        /// <returns>The <see cref="Vector3F"/> object this method creates.</returns>
        public Vector3F Clone()
        {
            return new Vector3F(this);
        }
        #endregion

        #region Public static parse methods
        /// <summary>
        /// Converts the specified string to a <see cref="Vector3F"/> equivalent.
        /// </summary>
        /// <param name="value">A string representation of a <see cref="Vector3F"/>.</param>
        /// <returns>Contains a <see cref="Vector3F"/> represented by <paramref name="value"/>.</returns>
        public static Vector3F Parse(string value)
        {
            Regex r = new Regex(@"\((?<x>.*),(?<y>.*),(?<z>.*)\)", RegexOptions.Singleline);
            Match m = r.Match(value);
            if (m.Success)
            {
                return new Vector3F(
                    float.Parse(m.Result("${x}")),
                    float.Parse(m.Result("${y}")),
                    float.Parse(m.Result("${z}"))
                    );
            }
            else
            {
                throw new Exception("Unsuccessful Match.");
            }
        }
        /// <summary>
        /// Converts the specified string to a <see cref="Vector3F"/> equivalent.
        /// </summary>
        /// <param name="value">A string representation of a <see cref="Vector3F"/>.</param>
        /// <param name="result">If conversion succeeded, contains a <see cref="Vector3F"/> represented by <paramref name="value"/>.</param>
        /// <returns><see langword="true"/> if conversion was successful; otherwise, <see langword="false"/>.</returns>
        public static bool TryParse(string value, out Vector3F result)
        {
            Regex r = new Regex(@"\((?<x>.*),(?<y>.*),(?<z>.*)\)", RegexOptions.Singleline);
            Match m = r.Match(value);
            if (m.Success)
            {
                result = new Vector3F(
                    float.Parse(m.Result("${x}")),
                    float.Parse(m.Result("${y}")),
                    float.Parse(m.Result("${z}"))
                    );

                return true;
            }

            result = Zero;
            return false;
        }
        #endregion

        #region Public static arithmetic

        #region Addition
        /// <summary>
        /// Adds a <see cref="Vector3F"/> to a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the sum.</returns>
        public static Vector3F Add(Vector3F left, Vector3F right)
        {
            return new Vector3F(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
        }
        /// <summary>
        /// Adds a scalar to a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the sum.</returns>
        public static Vector3F Add(Vector3F vector, float scalar)
        {
            return new Vector3F(vector.X + scalar, vector.Y + scalar, vector.Z + scalar);
        }
        /// <summary>
        /// Adds a <see cref="Vector3F"/> to a <see cref="Vector3F"/> storing the sum in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/></param>
        /// <param name="sum">A <see cref="Vector3F"/> to contain the sum.</param>
        public static void Add(Vector3F left, Vector3F right, ref Vector3F sum)
        {
            sum.X = left.X + right.X;
            sum.Y = left.Y + right.Y;
            sum.Z = left.Z + right.Z;
        }
        /// <summary>
        /// Adds a scalar to a <see cref="Vector3F"/> storing the sum in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <param name="sum">A <see cref="Vector3F"/> to contain the sum.</param>
        public static void Add(Vector3F vector, float scalar, ref Vector3F sum)
        {
            sum.X = vector.X + scalar;
            sum.Y = vector.Y + scalar;
            sum.Z = vector.Z + scalar;
        }
        #endregion

        #region Subtraction
        /// <summary>
        /// Subtracts a <see cref="Vector3F"/> from a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the difference.</returns>
        public static Vector3F Subtract(Vector3F left, Vector3F right)
        {
            return new Vector3F(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
        }
        /// <summary>
        /// Subtracts a scalar from a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the difference.</returns>
        public static Vector3F Subtract(Vector3F vector, float scalar)
        {
            return new Vector3F(vector.X - scalar, vector.Y - scalar, vector.Z - scalar);
        }
        /// <summary>
        /// Subtracts a <see cref="Vector3F"/> from a scalar.
        /// </summary>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the difference.</returns>
        public static Vector3F Subtract(float scalar, Vector3F vector)
        {
            return new Vector3F(scalar - vector.X, scalar - vector.Y, scalar - vector.Z);
        }
        /// <summary>
        /// Subtracts a <see cref="Vector3F"/> from a <see cref="Vector3F"/> storing the difference in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/></param>
        /// <param name="difference">A <see cref="Vector3F"/> to contain the difference.</param>
        public static void Subtract(Vector3F left, Vector3F right, ref Vector3F difference)
        {
            difference.X = left.X - right.X;
            difference.Y = left.Y - right.Y;
            difference.Z = left.Z - right.Z;
        }
        /// <summary>
        /// Subtracts a scalar from a <see cref="Vector3F"/> storing the difference in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <param name="difference">A <see cref="Vector3F"/> to contain the difference.</param>
        public static void Subtract(Vector3F vector, float scalar, ref Vector3F difference)
        {
            difference.X = vector.X - scalar;
            difference.Y = vector.Y - scalar;
            difference.Z = vector.Z - scalar;
        }
        /// <summary>
        /// Subtracts a <see cref="Vector3F"/> from a scalar storing the difference in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="difference">A <see cref="Vector3F"/> to contain the difference.</param>
        public static void Subtract(float scalar, Vector3F vector, ref Vector3F difference)
        {
            difference.X = scalar - vector.X;
            difference.Y = scalar - vector.Y;
            difference.Z = scalar - vector.Z;
        }
        #endregion

        #region Division
        /// <summary>
        /// Divides a <see cref="Vector3F"/> by a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the quotient.</returns>
        public static Vector3F Divide(Vector3F left, Vector3F right)
        {
            return new Vector3F(left.X / right.X, left.Y / right.Y, left.Z / right.Z);
        }
        /// <summary>
        /// Divides a <see cref="Vector3F"/> by a scalar.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the quotient.</returns>
        public static Vector3F Divide(Vector3F vector, float scalar)
        {
            return new Vector3F(vector.X / scalar, vector.Y / scalar, vector.Z / scalar);
        }
        /// <summary>
        /// Divides a scalar by a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the quotient.</returns>
        public static Vector3F Divide(float scalar, Vector3F vector)
        {
            return new Vector3F(scalar / vector.X, scalar / vector.Y, scalar / vector.Z);
        }
        /// <summary>
        /// Divides a <see cref="Vector3F"/> by a <see cref="Vector3F"/> storing the sum in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/></param>
        /// <param name="quotient">A <see cref="Vector3F"/> to contain the quotient.</param>
        public static void Divide(Vector3F left, Vector3F right, ref Vector3F quotient)
        {
            quotient.X = left.X / right.X;
            quotient.Y = left.Y / right.Y;
            quotient.Z = left.Z / right.Z;
        }
        /// <summary>
        /// Divides a <see cref="Vector3F"/> by a scalar storing the quotient in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <param name="quotient">A <see cref="Vector3F"/> to contain the quotient.</param>
        public static void Divide(Vector3F vector, float scalar, ref Vector3F quotient)
        {
            quotient.X = vector.X / scalar;
            quotient.Y = vector.Y / scalar;
            quotient.Z = vector.Z / scalar;
        }
        /// <summary>
        /// Divides a scalar by a <see cref="Vector3F"/> storing the quotient in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="quotient">A <see cref="Vector3F"/> to contain the quotient.</param>
        public static void Divide(float scalar, Vector3F vector, ref Vector3F quotient)
        {
            quotient.X = scalar / vector.X;
            quotient.Y = scalar / vector.Y;
            quotient.Z = scalar / vector.Z;
        }
        #endregion

        #region Multiplication
        /// <summary>
        /// Multiplies a <see cref="Vector3F"/> by a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the result.</returns>
        public static Vector3F Multiply(Vector3F left, Vector3F right)
        {
            return new Vector3F(left.X * right.X, left.Y * right.Y, left.Z * right.Z);
        }
        /// <summary>
        /// Multiplies a <see cref="Vector3F"/> by a scalar.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the result.</returns>
        public static Vector3F Multiply(Vector3F vector, float scalar)
        {
            return new Vector3F(vector.X * scalar, vector.Y * scalar, vector.Z * scalar);
        }
        /// <summary>
        /// Multiplies a <see cref="Vector3F"/> by a scalar storing the result in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <param name="result">A <see cref="Vector3F"/> to contain the result.</param>
        public static void Multiply(Vector3F vector, float scalar, ref Vector3F result)
        {
            result.X = vector.X * scalar;
            result.Y = vector.Y * scalar;
            result.Z = vector.Z * scalar;
        }
        /// <summary>
        /// Multiplies a <see cref="Vector3F"/> by a <see cref="Vector3F"/> storing the result in a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <param name="result">A <see cref="Vector3F"/> to contain the result.</param>
        public static void Multiply(Vector3F left, Vector3F right, ref Vector3F result)
        {
            result.X = left.X * right.X;
            result.Y = left.Y * right.Y;
            result.Z = left.Z * right.Z;
        }
        #endregion

        #region Linear algebra
        /// <summary>
        /// Calculates the dot product of two <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns>The dot product.</returns>
        public static float DotProduct(Vector3F left, Vector3F right)
        {
            return (left.X * right.X) + (left.Y * right.Y) + (left.Z * right.Z);
        }
        /// <summary>
        /// Calculates the cross product of two <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the cross product.</returns>
        public static Vector3F CrossProduct(Vector3F left, Vector3F right)
        {
            return new Vector3F(
                left.Y * right.Z - left.Z * right.Y,
                left.Z * right.X - left.X * right.Z,
                left.X * right.Y - left.Y * right.X);
        }
        /// <summary>
        /// Calculates the cross product of two vectors.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <param name="result">A <see cref="Vector3F"/> to contain the cross product.</param>
        public static void CrossProduct(Vector3F left, Vector3F right, ref Vector3F result)
        {
            result.X = left.Y * right.Z - left.Z * right.Y;
            result.Y = left.Z * right.X - left.X * right.Z;
            result.Z = left.X * right.Y - left.Y * right.X;
        }
        /// <summary>
        /// Negates a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the negated input.</returns>
        public static Vector3F Negate(Vector3F vector)
        {
            return new Vector3F(-vector.X, -vector.Y, -vector.Z);
        }
        /// <summary>
        /// Tests whether two <see cref="Vector3F"/> are approximately equal using Epsilon as the tolerance.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns><see langword="true"/> if the two vectors are approximately equal; otherwise, <see langword="false"/>.</returns>
        public static bool ApproxEqual(Vector3F left, Vector3F right)
        {
            return ApproxEqual(left, right, Constants.EpsilonF);
        }
        /// <summary>
        /// Tests whether two <see cref="Vector3F"/> are approximately equal using a given tolerance.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <param name="tolerance">A single-precision floating-point tolerance for determining approximate equality.</param>
        /// <returns><see langword="true"/> if the two vectors are approximately equal; otherwise, <see langword="false"/>.</returns>
        public static bool ApproxEqual(Vector3F left, Vector3F right, float tolerance)
        {
            return ((System.Math.Abs(left.X - right.X) <= tolerance) && (System.Math.Abs(left.Y - right.Y) <= tolerance) && (System.Math.Abs(left.Z - right.Z) <= tolerance));
        }
        #endregion

        #endregion

        #region Public methods
        /// <summary>
        /// Calculates the dot product of this <see cref="Vector3F"/> and another <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <returns>The dot product.</returns>
        public float DotProduct(Vector3F vector)
        {
            return (_x * vector.X) + (_y * vector.Y) + (_z * vector.Z);
        }
        /// <summary>
        /// Calculates the cross product of this <see cref="Vector3F"/> and another <see cref="Vector3F"/>>.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the cross product.</returns>
        public Vector3F CrossProduct(Vector3F vector)
        {
            return new Vector3F(
                _y * vector.Z - _z * vector.Y,
                _z * vector.X - _x * vector.Z,
                _x * vector.Y - _y * vector.X);
        }
        /// <summary>
        /// Scale the <see cref="Vector3F"/> so that it has a length of 1.0f.
        /// </summary>
        public void Normalize()
        {
            float length = GetLength();
            if (0.0f == length) { throw new DivideByZeroException("Trying to normalize a vector with length of zero."); }

            _x /= length;
            _y /= length;
            _z /= length;
        }
        /// <summary>
        /// Calculates the length of the <see cref="Vector3F"/>.
        /// </summary>
        /// <returns>The length.</returns>
        public float GetLength()
        {
            return (float)System.Math.Sqrt(_x * _x + _y * _y + _z * _z);
        }
        /// <summary>
        /// Calculates the squared length of the <see cref="Vector3F"/>.
        /// </summary>
        /// <returns>The squared length.</returns>
        public float GetLengthSquared()
        {
            return (_x * _x + _y * _y + _z * _z);
        }
        /// <summary>
        /// Clamps a <see cref="Vector3F"/> to 0.0f using a given tolerance value.
        /// </summary>
        /// <param name="tolerance">The tolerance to use.</param>
        /// <remarks>
        /// Components that are within the range of 0.0f and the given tolerance are set to 0.0f.
        /// </remarks>
        public void ClampZero(float tolerance)
        {
            _x = Functions.Clamp(_x, 0.0f, tolerance);
            _y = Functions.Clamp(_y, 0.0f, tolerance);
            _z = Functions.Clamp(_z, 0.0f, tolerance);
        }
        /// <summary>
        /// Clamps a <see cref="Vector3F"/> to 0.0f using the default tolerance value.
        /// </summary>
        /// <remarks>
        /// Components that are within the range of 0.0f and <see cref="MathFunctions.EpsilonF"/> are set to 0.0f.
        /// </remarks>
        public void ClampZero()
        {
            _x = Functions.Clamp(_x, 0.0f);
            _y = Functions.Clamp(_y, 0.0f);
            _z = Functions.Clamp(_z, 0.0f);
        }
        /// <summary>
        /// Determine if a <see cref="Vector3F"/> is within a sphere of a given radius in game units around this <see cref="Vector3F"/>. 
        /// </summary>
        /// <param name="vector">Target point</param>
        /// <param name="radius">Radius</param>
        /// <returns>True if the point is within the sphere, otherwise false</returns>
        /// <returns><see langword="true"/> if <paramref name="vector"/> is a <see cref="Vector3F"/> within the sphere; otherwise, <see langword="false"/>.</returns>
        public bool IsWithinGameUnitSizedSphere(Vector3F vector, float radius)
        {
            return Math.Abs((this - vector).GetLengthSquared() / (radius * radius)) < 1.0f;
        }
        /// <summary>
        /// Determine if a <see cref="Vector3F"/> is within a sphere of a given radius in feet around this <see cref="Vector3F"/>. 
        /// </summary>
        /// <param name="vector">Target point</param>
        /// <param name="radius">Radius</param>
        /// <returns>True if the point is within the sphere, otherwise false</returns>
        /// <returns><see langword="true"/> if <paramref name="vector"/> is a <see cref="Vector3F"/> within the sphere; otherwise, <see langword="false"/>.</returns>
        public bool IsWithinFeetSizedSphere(Vector3F vector, float radius)
        {
            return IsWithinGameUnitSizedSphere(vector, radius *= Constants.GameUnitsPerFoot);
        }
        /// <summary>
        /// Get the Euclidean distance between two positions represneted by this <see cref="Vector3F"/> and another <see cref="Vector3F"/> in feet.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <returns>The Euclidean distance</returns>
        public float GetDistanceTo(Vector3F vector)
        {
            return (this - vector).GetLength() / Constants.GameUnitsPerFoot;
        }
        #endregion

        #region System.Object overrides
        /// <summary>
        /// Returns the hash for this <see cref="Vector3F"/>.
        /// </summary>
        /// <returns>A 32-bit signed integer hash.</returns>
        public override int GetHashCode()
        {
            return _x.GetHashCode() ^ _y.GetHashCode() ^ _z.GetHashCode();
        }
        /// <summary>
        /// Returns a value indicating whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="obj">An object to compare to this instance.</param>
        /// <returns><see langword="true"/> if <paramref name="obj"/> is a <see cref="Vector3F"/> and has the same values as this instance; otherwise, <see langword="false"/>.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Vector3F v)
            {
                return ((_x == v.X) && (_y == v.Y) && (_z == v.Z));
            }
            return false;
        }
        /// <summary>
        /// Returns a value indicating whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="obj">An object to compare to this instance.</param>
        /// <returns><see langword="true"/> if <paramref name="obj"/> is a <see cref="Vector3F"/> and has the same values as this instance; otherwise, <see langword="false"/>.</returns>
        public static new bool Equals(object objA, object objB)
        {
            if (objA is Vector3F a && objA is Vector3F b)
            {
                return ((a.X == b.X) && (a.Y == b.Y) && (a.Z == b.Z));
            }
            return false;
        }
        /// <summary>
        /// Returns a string representation of this object.
        /// </summary>
        /// <returns>A string representation of this object.</returns>
        public override string ToString()
        {
            return $"X: {_x} Y: {_y} Z: {_z}";
        }
        #endregion

        #region Comparison operators
        /// <summary>
        /// Tests whether two specified vectors are equal.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns><see langword="true"/> if the two vectors are equal; otherwise, <see langword="false"/>.</returns>
        public static bool operator ==(Vector3F left, Vector3F right)
        {
            return Equals(left, right);
        }
        /// <summary>
        /// Tests whether two specified vectors are not equal.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns><see langword="true"/> if the two vectors are not equal; otherwise, <see langword="false"/>.</returns>
        public static bool operator !=(Vector3F left, Vector3F right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Tests if a vector's components are greater than another vector's components.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns><see langword="true"/> if the left-hand vector's components are greater than the right-hand vector's component; otherwise, <see langword="false"/>.</returns>
        public static bool operator >(Vector3F left, Vector3F right)
        {
            return (
                (left._x > right._x) &&
                (left._y > right._y) &&
                (left._z > right._z));
        }
        /// <summary>
        /// Tests if a vector's components are smaller than another vector's components.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns><see langword="true"/> if the left-hand vector's components are smaller than the right-hand vector's component; otherwise, <see langword="false"/>.</returns>
        public static bool operator <(Vector3F left, Vector3F right)
        {
            return (
                (left._x < right._x) &&
                (left._y < right._y) &&
                (left._z < right._z));
        }
        /// <summary>
        /// Tests if a vector's components are greater or equal than another vector's components.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns><see langword="true"/> if the left-hand vector's components are greater or equal than the right-hand vector's component; otherwise, <see langword="false"/>.</returns>
        public static bool operator >=(Vector3F left, Vector3F right)
        {
            return (
                (left._x >= right._x) &&
                (left._y >= right._y) &&
                (left._z >= right._z));
        }
        /// <summary>
        /// Tests if a vector's components are smaller or equal than another vector's components.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns><see langword="true"/> if the left-hand vector's components are smaller or equal than the right-hand vector's component; otherwise, <see langword="false"/>.</returns>
        public static bool operator <=(Vector3F left, Vector3F right)
        {
            return (
                (left._x <= right._x) &&
                (left._y <= right._y) &&
                (left._z <= right._z));
        }
        #endregion

        #region Unary operators
        /// <summary>
        /// <see cref="Vector3F.Negate"/>.
        /// </summary>
        public static Vector3F operator -(Vector3F vector)
        {
            return Negate(vector);
        }
        #endregion

        #region Binary Operators
        /// <summary>
        /// Adds two vectors.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> instance containing the sum.</returns>
        public static Vector3F operator +(Vector3F left, Vector3F right)
        {
            return Add(left, right);
        }
        /// <summary>
        /// Adds a vector and a scalar.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> instance containing the sum.</returns>
        public static Vector3F operator +(Vector3F vector, float scalar)
        {
            return Add(vector, scalar);
        }
        /// <summary>
        /// Adds a vector and a scalar.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> instance containing the sum.</returns>
        public static Vector3F operator +(float scalar, Vector3F vector)
        {
            return Add(vector, scalar);
        }
        /// <summary>
        /// Subtracts a vector from a vector.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> instance containing the difference.</returns>
        public static Vector3F operator -(Vector3F left, Vector3F right)
        {
            return Subtract(left, right);
        }
        /// <summary>
        /// Subtracts a scalar from a vector.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> instance containing the difference.</returns>
        public static Vector3F operator -(Vector3F vector, float scalar)
        {
            return Subtract(vector, scalar);
        }
        /// <summary>
        /// Subtracts a vector from a scalar.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> instance containing the difference.</returns>
        public static Vector3F operator -(float scalar, Vector3F vector)
        {
            return Subtract(scalar, vector);
        }
        /// <summary>
        /// Multiplies a <see cref="Vector3F"/> by a <see cref="Vector3F"/>.
        /// </summary>
        /// <param name="left">A <see cref="Vector3F"/>.</param>
        /// <param name="right">A <see cref="Vector3F"/>.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the result.</returns>
        public static Vector3F operator *(Vector3F left, Vector3F right)
        {
            return Multiply(left, right);
        }
        /// <summary>
        /// Multiplies a vector by a scalar.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the result.</returns>
        public static Vector3F operator *(Vector3F vector, float scalar)
        {
            return Multiply(vector, scalar);
        }
        /// <summary>
        /// Multiplies a vector by a scalar.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A single-precision floating-point number.</param>
        /// <returns>A new <see cref="Vector3F"/> containing the result.</returns>
        public static Vector3F operator *(float scalar, Vector3F vector)
        {
            return Multiply(vector, scalar);
        }
        /// <summary>
        /// Divides a vector by a scalar.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A scalar</param>
        /// <returns>A new <see cref="Vector3F"/> containing the quotient.</returns>
        public static Vector3F operator /(Vector3F vector, float scalar)
        {
            return Divide(vector, scalar);
        }
        /// <summary>
        /// Divides a scalar by a vector.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <param name="scalar">A scalar</param>
        /// <returns>A new <see cref="Vector3F"/> containing the quotient.</returns>
        public static Vector3F operator /(float scalar, Vector3F vector)
        {
            return Divide(scalar, vector);
        }
        #endregion

        #region Array indexing operator
        /// <summary>
        /// Indexer ( [x, y, z] ).
        /// </summary>
        public float this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return _x;
                    case 1:
                        return _y;
                    case 2:
                        return _z;
                    default:
                        throw new IndexOutOfRangeException();
                }
            }
            set
            {
                switch (index)
                {
                    case 0:
                        _x = value;
                        break;
                    case 1:
                        _y = value;
                        break;
                    case 2:
                        _z = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException();
                }
            }

        }
        #endregion

        #region Conversion operators
        /// <summary>
        /// Converts the vector to an array of single-precision floating point values.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <returns>An array of single-precision floating point values.</returns>
        public static explicit operator float[] (Vector3F vector)
        {
            float[] array = new float[3];
            array[0] = vector.X;
            array[1] = vector.Y;
            array[2] = vector.Z;
            return array;
        }
        /// <summary>
        /// Converts the vector to a <see cref="System.Collections.Generic.List"/> of single-precision floating point values.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <returns>A <see cref="System.Collections.Generic.List"/> of single-precision floating point values.</returns>
        public static explicit operator List<float>(Vector3F vector)
        {
            List<float> list = new List<float>(3)
            {
                vector.X,
                vector.Y,
                vector.Z
            };
            return list;
        }
        /// <summary>
        /// Converts the vector to a <see cref="System.Collections.Generic.LinkedList"/> of single-precision floating point values.
        /// </summary>
        /// <param name="vector">A <see cref="Vector3F"/>.</param>
        /// <returns>A <see cref="System.Collections.Generic.LinkedList"/> of single-precision floating point values.</returns>
        public static explicit operator LinkedList<float>(Vector3F vector)
        {
            LinkedList<float> list = new LinkedList<float>();
            list.AddLast(vector.X);
            list.AddLast(vector.Y);
            list.AddLast(vector.Z);
            return list;
        }
        #endregion
    }
}
