﻿using System;
using System.IO;
using System.Text;

namespace WarClient.Figleaf.Tables
{
    public class FigString2 : FigRecord
    {
        public Int32 Index { get; }
        public String Value { get; set; }
        public override String ToString() => Value;
        public FigString2(FigleafDB db, Int32 index) : base(db) => Index = index;
    }

    public class FigStringTable2 : FigTable<FigString2>
    {
        public UInt32 Unk1 { get; set; }
        private const Int32 HeaderPosition = 0x14;

        public FigStringTable2(FigleafDB db) : base(db)
        {
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;

            foreach (FigString2 entry in Records)
            {
                if (!String.IsNullOrEmpty(entry.Value))
                    writer.Write(Encoding.GetEncoding(437).GetBytes(entry.Value), 0, entry.Value.Length);
                writer.Write((Byte)0);
            }
            var endPos = writer.BaseStream.Position;
            writer.BaseStream.Position = HeaderPosition;
            writer.Write(Unk1);
            writer.Write((UInt32)(Records.Count));
            writer.Write((UInt32)(Records.Count > 0 ? pos : 0));
            writer.Write((UInt32)(endPos - pos));
            writer.BaseStream.Position = endPos;
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            Unk1 = reader.ReadUInt32();
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();
            var str = new Byte[0xFFFF];

            reader.BaseStream.Position = Offset;

            for (var i = 0; i < EntryCount; i++)
            {
                var offset = reader.BaseStream.Position;

                var size = 0;
                for (size = 0; size < 0xFFFF; size++)
                {
                    var c = reader.ReadByte();
                    if (c == '\0')
                        break;
                    str[size] = c;
                }

                Records.Add(new FigString2(_db, i)
                {
                    Value = Encoding.GetEncoding(437).GetString(str, 0, size),
                });
            }
        }
    }
}
